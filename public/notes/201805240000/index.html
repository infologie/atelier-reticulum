<!DOCTYPE html>
<html lang="fr">
<head>
  
    <title>Technologies intellectuelles :: Infologie</title>
  
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">
<meta name="description" content="\marginnote[0.4cm]{\textbf{Post print de} Perret, Arthur. « Technologies intellectuelles ». \textit{Abécédaire des mondes lettrés}. 2018. \url{http://abecedaire.enssib.fr/t/technologies-intellectuelles/notices/138}}Des fondations anciennes aux explorations les plus audacieuses, l’écriture constitue l’architecture même des mondes lettrés. C’est un outil indissociable de la pensée, dont le développement est profondément lié à l’histoire de notre rationalité : une technologie de l’intellect.
Au fil du XIX^e^ siècle puis du XX^e^, l’écriture a fait l’objet d’études approfondies dans toutes ses dimensions : systèmes de signes, supports d’inscription, pratiques, systèmes de pensée." />
<meta name="keywords" content="" />
<meta name="robots" content="noodp" />
<link rel="canonical" href="/notes/201805240000/" />




<link rel="stylesheet" href="assets/style.css">

  <link rel="stylesheet" href="assets/red.css">



<link rel="stylesheet" href="style.css">


<link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/apple-touch-icon-144-precomposed.png">

  <link rel="shortcut icon" href="img/favicon/favicon.png">



<meta name="twitter:card" content="summary" />

  <meta name="twitter:site" content="@arthurperret" />

<meta name="twitter:creator" content="Arthur Perret" />


<meta property="og:locale" content="fr" />
<meta property="og:type" content="article" />
<meta property="og:title" content="Technologies intellectuelles :: Infologie">
<meta property="og:description" content="\marginnote[0.4cm]{\textbf{Post print de} Perret, Arthur. « Technologies intellectuelles ». \textit{Abécédaire des mondes lettrés}. 2018. \url{http://abecedaire.enssib.fr/t/technologies-intellectuelles/notices/138}}Des fondations anciennes aux explorations les plus audacieuses, l’écriture constitue l’architecture même des mondes lettrés. C’est un outil indissociable de la pensée, dont le développement est profondément lié à l’histoire de notre rationalité : une technologie de l’intellect.
Au fil du XIX^e^ siècle puis du XX^e^, l’écriture a fait l’objet d’études approfondies dans toutes ses dimensions : systèmes de signes, supports d’inscription, pratiques, systèmes de pensée." />
<meta property="og:url" content="/notes/201805240000/" />
<meta property="og:site_name" content="Technologies intellectuelles" />

  
    <meta property="og:image" content="img/favicon/favicon.png">
  

<meta property="og:image:width" content="2048">
<meta property="og:image:height" content="1024">


  <meta property="article:published_time" content="2018-05-24 00:00:00 &#43;0000 UTC" />












</head>
<body class="">


<div class="container center headings--one-size">

  <header class="header">
  <div class="header__inner">
    <div class="header__logo">
      <a href="/">
  <div class="logo">
    INFOLOGIE
  </div>
</a>

    </div>
    <div class="menu-trigger">menu</div>
  </div>
  
    <nav class="menu">
  <ul class="menu__inner menu__inner--desktop">
    
      
        
          <li><a href="/a-propos">À propos</a></li>
        
      
      
    

    
  </ul>

  <ul class="menu__inner menu__inner--mobile">
    
      
        <li><a href="/a-propos">À propos</a></li>
      
    
    
  </ul>
</nav>

  
</header>


  <div class="content">
    
<div class="post">
  <h1 class="post-title">
    <a href="/notes/201805240000/">Technologies intellectuelles</a></h1>
  <div class="post-meta">
    
      <span class="post-date">
        2018-05-24 
      </span>
    
    
    <span class="post-author">::
      Arthur Perret
    </span>
    
  </div>

  

  

  

  <div class="post-content"><div>
        <p>\marginnote[0.4cm]{\textbf{Post print de} Perret, Arthur. « Technologies intellectuelles ». \textit{Abécédaire des mondes lettrés}. 2018. \url{http://abecedaire.enssib.fr/t/technologies-intellectuelles/notices/138}}Des fondations anciennes aux explorations les plus audacieuses, l’écriture constitue l’architecture même des mondes lettrés. C’est un outil indissociable de la pensée, dont le développement est profondément lié à l’histoire de notre rationalité : une <em>technologie de l’intellect</em>.</p>
<p>Au fil du XIX^e^ siècle puis du XX^e^, l’écriture a fait l’objet d’études approfondies dans toutes ses dimensions : systèmes de signes, supports d’inscription, pratiques, systèmes de pensée. L’anthropologie et l’histoire des sciences ont fait émerger les logiques sous-jacentes de nos dispositifs d’écriture, en identifiant leurs apports indéniables à notre intellect. Au niveau le plus élémentaire, l’inscription matérielle du langage permet de libérer des capacités mobilisées par exemple par la mémorisation et de les dédier à des processus nouveaux : la réflexivité, l’analyse, la critique. Le développement de l’écriture montre que ce phénomène s’est renouvelé tout au long de notre histoire : la liste décuple les possibilités de gestion et de manipulation de l’information ; le tableau et la carte les accroissent encore ; le rouleau, le codex et l’imprimerie permettent de déployer, approfondir et accélérer le partage des connaissances ; la base de données, l’écran et le réseau renforcent le potentiel heuristique de toutes ces activités. Comme l’écrit J. Goody dans <em>La Raison graphique</em>, ces technologies contribuent à l’apparition effective de « nouvelles aptitudes intellectuelles » [@goody1979, 193]. Cette réflexion pionnière occupe toujours les sciences de l’information, tant par l’outillage conceptuel qu’elle nous a livré (en premier lieu la notion de technologie de l’intellect) que par les questions laissées en suspens.</p>
<p>L’étude de l’écriture interroge le rôle de l’outillage dans le travail intellectuel, et de façon plus large les rapports entre culture et technique. Pour Goody, les mécanismes de l’écriture en tant que technologie de l’intellect constituent le socle d’une réfutation en creux du « grand partage » ethnique ; sa contribution à un humanisme de tendance universaliste nourrit une grille de lecture bien plus pertinente des continuités et des ruptures de nos sociétés que les dichotomies dépassées entre « primitifs » et « civilisés ». Mais c’est avant tout du côté de l’épistémologie que la notion se révèle le plus naturellement féconde. Au-delà d’une vision restreinte au seul développement récent de l’informatique, les travaux sur l’écriture (et la réflexivité qu’elle permet) suggèrent que le principe général des technologies de l’intellect est autant une affaire d’encre et de plomb que de pixels et de balises, donc qu’il existe une cohérence dans nos modes d’instrumentation du savoir et d’élaboration de la connaissance, de l’imprimé à l’écran. Gravure sur tablette de cire, encyclopédie imprimée, page Web : texte, hypertexte, architexte. Cette cohérence représente un postulat essentiel, puisqu’elle fonde une pensée critique du numérique indispensable face aux discours approximatifs, aux idéologies simplificatrices et à l’impensé général… de la pensée.</p>
<p>Les technologies de l’intellect constituent le ciment de nos sociétés modernes, autrefois partiellement lettrées et aujourd’hui entièrement écrites. Nous les retrouvons en effet dans une grande partie de nos activités intellectuelles et de nos rapports sociaux, qu’il s’agisse de modes d’expression, de mémorisation ou d’organisation, une quasi-universalité induite par des dispositifs d’information et de communication toujours plus présents. C’est pourquoi leurs usages forment un terrain extrêmement important pour le travail des sciences humaines et sociales. C’est aussi la raison pour laquelle elles occupent une place importante dans l’élaboration de cadres théoriques relatifs à ces mêmes usages. L’épistémologie construite en suivant le fil rouge des technologies de l’intellect résulte d’une conjugaison permanente et nécessaire du terrain et de la théorie, aussi bien au présent qu’au passé. Elle répond en cela aux exigences d’un humanisme scientifique toujours d’actualité.</p>
<h1 id="référence--">Référence {-}<a href="#référence--" class="hanchor" ariaLabel="Anchor">&#8983;</a> </h1>

      </div></div>

  
  
<div class="pagination">
    <div class="pagination__title">
        <span class="pagination__title-h"></span>
        <hr />
    </div>
    <div class="pagination__buttons">
        
        <span class="button previous">
            <a href="/notes/201806140000/">
                <span class="button__icon">←</span>
                <span class="button__text">Matière à pensées : outils d’édition et médiation de la créativité</span>
            </a>
        </span>
        
        
        <span class="button next">
            <a href="/notes/201803261410/">
                <span class="button__text">Bibliothèque</span>
                <span class="button__icon">→</span>
            </a>
        </span>
        
    </div>
</div>

  

  

</div>

  </div>

  
    <footer class="footer">
  <div class="footer__inner">
      <div class="copyright">
        <span>© Arthur Perret 2020 :: Site réalisé avec <a href="http://gohugo.io">Hugo</a> :: Thème Terminal par <a href="https://twitter.com/panr">panr</a></span>
      </div>
  </div>
</footer>

<script src="assets/main.js"></script>
<script src="assets/prism.js"></script>





  
</div>

</body>
</html>
