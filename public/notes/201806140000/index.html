<!DOCTYPE html>
<html lang="fr">
<head>
  
    <title>Matière à pensées : outils d’édition et médiation de la créativité :: Infologie</title>
  
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">
<meta name="description" content="Introduction Cet article s’attache à démontrer le potentiel heuristique de l’édition numérique en matière d’épistémologie des sciences de l’information et de la communication (SIC). De Word à InDesign en passant par LaTeX mais aussi les éditeurs de code et les systèmes d’information géographique (SIG), la création de documents numériques se déploie au sein de dispositifs aux apparences similaires mais dont les logiques présentent des variations hautement significatives. Ces dispositifs illustrent le passage d’un régime documentaire à un autre : de la reproduction du paradigme de l’imprimé à l’émergence de nouvelles boucles de matérialisation au plus près de la technologie de l’intellect." />
<meta name="keywords" content="" />
<meta name="robots" content="noodp" />
<link rel="canonical" href="/notes/201806140000/" />




<link rel="stylesheet" href="assets/style.css">

  <link rel="stylesheet" href="assets/red.css">



<link rel="stylesheet" href="style.css">


<link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/apple-touch-icon-144-precomposed.png">

  <link rel="shortcut icon" href="img/favicon/favicon.png">



<meta name="twitter:card" content="summary" />

  <meta name="twitter:site" content="@arthurperret" />

<meta name="twitter:creator" content="Arthur Perret" />


<meta property="og:locale" content="fr" />
<meta property="og:type" content="article" />
<meta property="og:title" content="Matière à pensées : outils d’édition et médiation de la créativité :: Infologie">
<meta property="og:description" content="Introduction Cet article s’attache à démontrer le potentiel heuristique de l’édition numérique en matière d’épistémologie des sciences de l’information et de la communication (SIC). De Word à InDesign en passant par LaTeX mais aussi les éditeurs de code et les systèmes d’information géographique (SIG), la création de documents numériques se déploie au sein de dispositifs aux apparences similaires mais dont les logiques présentent des variations hautement significatives. Ces dispositifs illustrent le passage d’un régime documentaire à un autre : de la reproduction du paradigme de l’imprimé à l’émergence de nouvelles boucles de matérialisation au plus près de la technologie de l’intellect." />
<meta property="og:url" content="/notes/201806140000/" />
<meta property="og:site_name" content="Matière à pensées : outils d’édition et médiation de la créativité" />

  
    <meta property="og:image" content="img/favicon/favicon.png">
  

<meta property="og:image:width" content="2048">
<meta property="og:image:height" content="1024">


  <meta property="article:published_time" content="2018-06-14 00:00:00 &#43;0000 UTC" />












</head>
<body class="">


<div class="container center headings--one-size">

  <header class="header">
  <div class="header__inner">
    <div class="header__logo">
      <a href="/">
  <div class="logo">
    INFOLOGIE
  </div>
</a>

    </div>
    <div class="menu-trigger">menu</div>
  </div>
  
    <nav class="menu">
  <ul class="menu__inner menu__inner--desktop">
    
      
        
          <li><a href="/a-propos">À propos</a></li>
        
      
      
    

    
  </ul>

  <ul class="menu__inner menu__inner--mobile">
    
      
        <li><a href="/a-propos">À propos</a></li>
      
    
    
  </ul>
</nav>

  
</header>


  <div class="content">
    
<div class="post">
  <h1 class="post-title">
    <a href="/notes/201806140000/">Matière à pensées : outils d’édition et médiation de la créativité</a></h1>
  <div class="post-meta">
    
      <span class="post-date">
        2018-06-14 
      </span>
    
    
    <span class="post-author">::
      Arthur Perret
    </span>
    
  </div>

  

  

  

  <div class="post-content"><div>
        <h1 id="introduction">Introduction<a href="#introduction" class="hanchor" ariaLabel="Anchor">&#8983;</a> </h1>
<p>Cet article s’attache à démontrer le potentiel heuristique de l’édition numérique en matière d’épistémologie des sciences de l’information et de la communication (SIC). De Word à InDesign en passant par LaTeX mais aussi les éditeurs de code et les systèmes d’information géographique (SIG), la création de documents numériques se déploie au sein de dispositifs aux apparences similaires mais dont les logiques présentent des variations hautement significatives. Ces dispositifs illustrent le passage d’un régime documentaire à un autre : de la reproduction du paradigme de l’imprimé à l’émergence de nouvelles boucles de matérialisation au plus près de la technologie de l’intellect. Nous concentrons notre analyse sur ces dernières, en examinant spécifiquement le rapport entre créativité et création. Inscrite dans une histoire longue des médiations du texte, de l’image et de la pensée, notre réflexion se rattache à l’exploration critique des liens entre humain et machine, culture et technique.</p>
<p>Notre problématique est la suivante : comment les nouvelles médiations du travail intellectuel favorisent-elles la créativité ?</p>
<p>Dans les paragraphes qui suivent, nous décrivons un processus de matérialisation dynamique observable dans la pratique d’un vaste ensemble de logiciels, de la programmation à la visualisation. Ce processus transforme la pratique éditoriale en une méthode exploratoire au potentiel heuristique inédit. C’est une nouvelle médiation du travail intellectuel qui encourage la répétition des essais, la variation du paramétrage, la conception de solutions innovantes et donc la créativité, y compris dans des cadres scientifiques ou industriels a priori fortement normés. Nous examinons les modalités suivant lesquelles cette innovation se déploie en réalisant une analyse des technologies, interfaces et usages liés aux outils d’édition ; il s’agit notamment de questionner les interactions, la représentation de l’information et le degré de littératie numérique sollicité pour évaluer précisément les conditions d’émergence de la créativité.</p>
<h1 id="édition">Édition<a href="#édition" class="hanchor" ariaLabel="Anchor">&#8983;</a> </h1>
<p>La place grandissante du calcul dans nos pratiques d’écriture et de lecture nous force à réévaluer de plus en plus régulièrement nos cadres de pensée relatifs aux technologies de l’information et de la communication (TIC) [@pedauque2006; @bachimont2007]. En particulier, notre théorie des processus documentaires fait face à un écosystème en pleine évolution, lequel nous pousse à raffiner notre approche de la technique. Le concept de document, structurant pendant tout le XX^e^ siècle, se révèle inégalement opérant en ce qui concerne les phénomènes numériques. Réactualisé avec une extrême fécondité en ce qui concerne tout un pan de l’analyse relative au Web, en particulier sur les enjeux économiques et sociaux [@pedauque2007], il pose des difficultés épistémologiques dès lors qu’on aborde certains mécanismes plus fondamentaux liés à l’information, ainsi que nous allons le voir plus loin.</p>
<p>L’édition constitue un terrain privilégié pour l’observation de ce changement de paradigme. C’est une industrie qui se définit traditionnellement par l’élaboration du livre et de ses dérivés, des documents stables qui sont conçus en procédant à partir de la page comme unité de base – une logique façonnée par cinq siècles de règne de l’imprimé. Avant toute autre chose, il convient de rappeler que si la langue anglaise fait la distinction entre l’édition en tant que filière (<em>publishing</em>) et l’élaboration effective du document (<em>editing</em>), nous avons importé la bureautique anglo-saxonne sans modifier notre propre vocabulaire. Par conséquent, le terme français d’édition a acquis une certaine polysémie. Il est possible d’en donner une définition unique : éditer, c’est orchestrer l’élaboration du document. Toutefois, dans le contexte de cet article c’est la seconde dimension qui nous intéresse : éditer au sens de créer et manipuler des fichiers ; éditeur au sens de logiciel d’édition ; édition au sens de pratiques d’écriture, entre conception, rédaction et programmation.</p>
<p>L’édition repose en grande partie sur des méthodes, modèles ou schémas reproductibles ; on n’y crée que rarement à partir de rien et l’informatisation a très largement favorisé cela grâce à la copie et à l’automatisation. Les logiciels permettant d’éditer des documents numériques présentent de façon quasiment universelle des fonctionnalités qui soutiennent la conception de processus stables et de maquettes réutilisables. Parmi ces fonctionnalités, on notera l’import et l’export de paramétrages divers (réglages d’interface, gabarits) ; le pré-chargement de données et de librairies distantes via Internet ; la modélisation des tâches sous forme d’algorithmes plus ou moins complexes. Comme pour le verbe <em>éditer</em>, la bureautique attache un sens plus prosaïque au verbe <em>créer</em> que sa définition préalable en français : il s’agit simplement d’une génération de document, dont on voit qu’elle fait l’objet d’une prise en charge qui est profondément intégrée à la proposition technique de base des outils d’édition. Dans ce contexte, <em>quid</em> de la créativité ?</p>
<p>Le Trésor de la langue française définit le verbe <em>créer</em> ainsi : « concevoir, imaginer quelque chose de nouveau, donner une forme originale à quelque chose ». L’histoire nous a appris que la technique se raffine au fil des usages ; en ce qui concerne l’édition, ce constat est vérifié par l’observation des pratiques actuelles : la dimension créative semble se déplacer progressivement vers des utilisations plus avancées que la simple génération de document, s’appréciant désormais à l’aune d’une certaine virtuosité dans la combinatoire des données, la capacité à réécrire l’outil lui-même ou encore l’inventivité formelle.</p>
<p>Ce constat est vrai dans l’édition dite classique, où la production de formats multiples nécessite de repenser les pratiques. La filière passe lentement d’une conception ancrée dans la page à une maîtrise plus complète de la notion de flux [@tangaro2017]. En pratique, il s’agit de circuler entre différentes technologies : de la maquette graphique au balisage, de la bureautique au code. Le constat se vérifie aussi dans les méthodes de production visuelle au sens large : cartographie et visualisation puisent dans le traitement de données et les langages de programmation, que cela soit réalisé avec des interfaces de bureautique traditionnelle comme dans le cas des SIG ou bien avec des technologies du Web dans le cas des différentes librairies Javascript ayant émergé ces dernières années (Leaflet et D3, entre autres). Quelles que soient les formes d’édition, la simple création de documents est devenue une affaire triviale car la prise en main des outils est de plus en plus accessible. En principe, il s’agit toujours de la maîtrise des possibilités techniques, de l’articulation des différents outils ou encore d’une bonne connaissance de l’état de l’art qui pavent la route vers une plus grande créativité.</p>
<p>Cependant, certains environnements laissent entrevoir des modalités de création qui se situent paradoxalement à un niveau beaucoup plus élémentaire d’interaction entre humain et machine, démontrant une co-construction profonde et ancienne autour de ce que l’on appelle les technologies intellectuelles.</p>
<p>Les programmes à interface de type WYSIWYG comme Word ou InDesign s’inscrivent dans la métaphore du bureau ou de l’établi : nous matérialisons un document par un assemblage dont les étapes miment la fabrication du document imprimé. Le couple souris-interface graphique a favorisé le développement de cette informatique accessible au grand public au-delà des informaticiens, soutenu par un effet diligence observable aussi bien dans les appellations que les codes visuels. Comme pour toute innovation, cet effet tend à s’estomper avec le temps : le déclin du skeuomorphisme, un style de conception qui donne aux programmes l’apparence d’objets physiques, en est un bon exemple. Néanmoins, nous opérons toujours avec des références datant du début du XX^e^ siècle : bureau, fichier, copier-coller, etc.</p>
<p>Antérieurement et parallèlement, d’autres modes d’édition existent, qui offrent une perspective intéressante sur notre problématique. Un programme tel que LaTeX repose sur la fabrication de plusieurs documents successifs : d’abord un document maître (souvent appelé source) qui concentre tout le travail d’édition, puis un document qui résulte du traitement de cette source par le programme. Dans un autre registre, les pages Web sont généralement fabriquées dans un éditeur mais interprétées et affichées dans un navigateur Internet. Entre ces deux cas de figure existe encore une autre variante dont font partie les outils d’édition cartographique, lesquels coordonnent simultanément de nombreux fichiers de types différents, avec production d’un ou plusieurs documents à la clé.</p>
<p>Ces trois types d’environnements de travail ont une caractéristique commune qui les distinguent des WYSIWYG : le passage par un affichage du document final est parfaitement facultatif, car tout peut être réalisé via des interfaces textuelles – éditeur de texte brut, éditeur de code plus sophistiqué, ligne de commande, etc. Cependant, la possibilité d’affichage existe et elle crée une décorrélation intéressante. Ce n’est pas nécessairement une séparation entre fond et forme, puisqu’un même fichier HTML ou LaTeX peut contenir à la fois des informations de nature sémantique et stylistique. Ce n’est pas non plus une séparation stricte entre document édité et document produit. Un logiciel de cartographie classique comme QGIS ou ArcGIS, par exemple, brouille les cartes : il présente une version de la carte en cours d’une façon qui évoque le WYSIWYG sans en être tout à fait, puisque l’appareil critique est généralement ajouté dans une étape séparée du travail sur la représentation graphique, laquelle constitue l’interface principale. La décorrélation a en fait lieu entre objets documentaires : les documents sur lesquels on travaille, les documents que l’on produit et une image de ces derniers au milieu.</p>
<p>Quelle est la nature de cette « image » ? D’ores et déjà, on a l’intuition qu’il s’agit d’un élément clé dans notre rapport à ces pratiques nouvelles d’édition et qu’elle nécessite un effort de description à la fois technique et conceptuel tout à fait particulier.</p>
<h1 id="matérialisation">Matérialisation<a href="#matérialisation" class="hanchor" ariaLabel="Anchor">&#8983;</a> </h1>
<p>Le rapport à cette image varie en fonction de la technologie utilisée. Espace d’écriture et espace de visualisation sont souvent côte-à-côte, mais certains programmes mettent à jour l’aperçu du document final de façon automatique, tandis que d’autres nécessitent une action pour cela. La distinction est importante. Un document écrit en LaTeX doit être compilé pour que le PDF s’affiche, que l’on travaille localement ou par le biais d’un service en ligne. En revanche, la majorité des éditeurs de code permettent un rendu en temps réel des modifications apportées à un fichier HTML ou Markdown. De la même façon, un SIG calcule généralement le rendu des cartes à chaque interaction. Ce sont ces matérialisations, ces rendus qui se révèlent les plus féconds en matière d’épistémologie. En effet, les similitudes de ces différents environnements ne peuvent occulter la spécificité fascinante qu’offre cet objet-là. Elle tient en une constatation : ces matérialisations qui se présentent à nous n’ont aucune existence documentaire.</p>
<p>La définition du document dont nous avons hérité au tournant des années 2000 a été façonnée par plusieurs décennies de tradition théorique, de Paul Otlet au collectif Pédauque, en passant par des auteurs fondamentaux comme Briet, Pagès, Estivals, Escarpit, Meyriat et Buckland, pour ne citer qu’eux. Notion avant tout fonctionnelle, elle nous permet d’appréhender la trace écrite comme une information enregistrée en vue de sa transmission. Or les logiciels d’édition que nous étudions ici nous mettent face à un objet dont cette définition ne peut se saisir.</p>
<p>À première vue, il y a bien une inscription sur un support : des pixels allumés sur un écran. Il y a donc manifestement quelque chose qui pourrait être qualifié d’image, de représentation ou de visualisation, quelque chose qui pourrait être documentarisé par capture d’écran, sauvegarde ou export répétés, permettant la circulation dans le volume et la temporalité d’une série de traces. Mais ce qui nous frappe ici, c’est précisément le fait que cet objet se présente et disparaît sans laisser de traces. Il n’y a pas de document, seulement une matérialisation par la machine en réponse à la matérialisation mentale, une image de l’image que nous nous faisons du document à venir – une « image d’image » [@perret2017]. La réalité de l’objet n’est pas en cause, nous en saisissons l’existence aussi bien par intuition que par analyse ; une opération mentale a eu lieu, matérialisée par une technologie de l’intellect mettant en jeu un support et des informations tangibles [@bachimont2010; @jacob1992]. Il y a cependant un décalage apparent entre nos repères théoriques et cette matérialisation qui nous invite à en chercher de nouveaux.</p>
<p>Qu’est-ce qu’un rendu ? Ce n’est pas un document : c’est une inscription de pixels. Il est à l’écran ce que l’encre est au papier : on allume un pixel comme on ferait un point sur une page. Une API de rendu orchestre l’utilisation de ces pixels en coordonnant logiciel et matériel (on peut citer OpenGL pour le logiciel libre, DirectX pour Microsoft ou encore Metal pour Apple, parmi d’autres) : c’est un tiers indispensable entre programme et écran, un tiers inédit entre système de signes et support pour ce qui est de l’écriture. Derrière cette notion informatique d’une certaine complexité se cache une constatation étonnante : il faut parfois chercher en dehors de ses raffinements syntaxiques nouveaux pour revenir aux fondamentaux de l’écriture tout court. Il y a une sorte de parenté intuitive entre l’écriture que nous connaissons et cette opération primitive d’inscription sur un support, bien plus qu’avec la compilation algorithmique de quelques centaines de lignes de code.</p>
<p>Le rendu d’une page Web, celui d’une carte et l’affichage du PDF dans un éditeur LaTeX sont des cas assez différents. Un programme comme LaTeX se comporte de manière finalement assez classique en termes documentaires : l’affichage de l’éditeur montre un fichier PDF produit à partir d’un fichier texte. Le langage HTML nous entraîne plus loin : l’affichage de l’éditeur est un rendu produit par un moteur Web, c’est-à-dire une image de texte (laquelle procède d’un document écrit). La cartographie numérique enfin nous pose une difficulté d’appréhension encore plus grande. D’une part, le logiciel matérialise quelque chose qui n’a pas d’origine documentaire définie : le matériau de base est de l’information géographique, c’est-à-dire une constellation de données hétérogènes (texte, vecteurs, statistiques, position de l’interface graphique) et d’emplacements informatiques distincts (fichiers locaux, librairie de données distante, paramètres du logiciel). D’autre part, cette matérialisation ne correspond pas à un document final existant – même si celui-ci était créé par la suite, nous avons vu que son processus de production fragmenté crée nécessairement des différences avec le rendu.</p>
<p>Nous sommes donc en présence d’un objet particulier. Nous avons vu qu’il est difficile à appréhender en termes documentaires. Sur les questions d’écriture, les outils conceptuels des SIC les plus prometteurs de ces vingt dernières années sont inégalement opérants. Fondamentalement, il s’agit plutôt d’un problème de sémiotique mais cette approche est presque entièrement à construire tant le décalage est grand entre l’héritage théorique du XX^e^ siècle et les défis concrets posés par les objets informatiques. Reste alors la question des apports intellectuels. Ici se rouvre le champ des possibles, car une lignée conceptuelle forte se dessine depuis plusieurs décennies au croisement de l’anthropologie, de l’histoire et de la philosophie : l’idée d’une co-construction de la culture et de la technique qui trouve une résonance particulière dans le concept de technologie de l’intellect.</p>
<h1 id="créativité">Créativité<a href="#créativité" class="hanchor" ariaLabel="Anchor">&#8983;</a> </h1>
<p>L’idée qu’un rendu matérialise quelque chose n’est ni pléonastique ni anodine. Le caractère à la fois insatisfaisant et dynamique de cet objet est crucial en ce qu’il nous met sur la piste de sa relation avec notre intellect. Spécifiquement, la réduction inhérente au paramétrage et à ses difficultés font que l’image anticipée par l’usager ne correspond pas forcément à l’image proposée par la machine. Néanmoins, la réactivité du rendu permet des aller-retours constants et c’est précisément l’intérêt du dispositif : sa capacité à puiser dans les logiques d’accélération inhérentes aux TIC pour mettre en place un rapport intellectuel privilégié entre opérateur et outil. Grâce à la vitesse de calcul et d’affichage élevée des équipements actuels, cette matérialisation répond presque instantanément à celle de l’esprit. Les capacités informatiques sont devenues telles que la métaphore du bureau est dépassée au profit de celle du partenaire de travail, avec un phénomène d’entraînement qui s’observe empiriquement, à mi-chemin entre frustration et émulation.</p>
<p>Ce que nous voyons là, ce sont les conditions d’une créativité renouvelée, liée à une évolution de la participation des outils au travail intellectuel.</p>
<p>Les intermédiaires essentiels entre nous et les technologies de l’intellect sont d’abord d’ordre sensoriel : ouïe pour le langage, vue pour l’écriture. En ce qui concerne la machine, c’est l’interface qui joue ce rôle, c’est-à-dire un tiers médiateur organisant des modalités d’interaction soigneusement conçues. De façon intéressante, ces interactions empruntent beaucoup au régime des sens ; un exemple frappant est celui des différentes commandes des programmes informatiques, dont la documentation révèle fréquemment une conception marquée par ces mêmes sens (en Perl ou Python par exemple : <em>seek</em>, <em>listen</em>, etc.).</p>
<p>C’est un perfectionnement de cette médiation que nous observons dans les logiciels d’édition, une évolution de la <em>technè</em> éditoriale qui nous positionne plus près que jamais de son moteur sous-jacent : l’écriture. Ce phénomène de matérialisations conjointes orchestrées par des interfaces est en effet régi par l’écriture : l’abstraction qui prend place dans un cerveau ou un ordinateur se construisent chacune à partir d’informations, de données et de documents façonnés par la culture écrite. Or cette écriture est d’une sophistication croissante.</p>
<p>L’histoire de l’écriture est celle d’une série de réponses à des problèmes fondamentaux de traitement, navigation ou modélisation de l’information [@goody1986; @goody1979; @robert2010]. Prenons l’exemple de la cartographie : elle représente un moyen d’extraire du sens à partir de données sur la base d’une logique qui puise dans les formalisations de la raison graphique ; cela passe par la production d’un objet qui constitue un tour de force épistémologique, puisque la carte produit plus de sens que les données d’origine tout en opérant une réduction importante de l’information [@dagognet1999]. L’information géographique (sous sa forme numérique) constitue un passage à l’échelle en matière de complexité mais les SIG nous libèrent d’une certaine pesanteur documentaire. P. Otlet écrit dans son <em>Traité de documentation</em> : « Le Document offre de la Réalité une image à la sixième dérivation » [@otlet1934]. Cette boucle de matérialisations mentales et logicielles qui s’établit dans la pratique des SIG puise dans une information géographique exprimée sous forme d’écriture fragmentée, recombinée. En quelque sorte, les SIG escamotent le document et nous rapprochent des quatre dérivations antérieures selon le schéma d’Otlet : la science, le langage, les sens et l’intelligence. Le dispositif génère ainsi un espace privilégié pour le travail intellectuel et plus spécifiquement l’expérimentation.</p>
<p>Reprenons le chemin du terrain : quelles différences peut-on relever dans la pratique des logiciels d’édition ayant occasionné nos observations jusqu’ici ?</p>
<p>Les éditeurs de code, qu’ils soient spécifiques à la pratique d’un langage ou un autre, agencent document source et rendu côte-à-côte. La logique de l’aller-retour est présente, avec toutefois un paramètre déterminant : la simultanéité du rendu. Dès la première compilation d’un document en LaTeX, le résultat est une entité autonome dont le format ne changera sensiblement pas d’ici à sa version finale car il s’agit d’un fichier PDF. Par ailleurs, un ensemble de fichiers auxiliaires se densifie progressivement autour afin d’héberger des opérations qui ne sont pas systématiquement renouvelées à chaque compilation : table des matières, bibliographie, index, glossaire, etc. Le tout est dûment consigné dans un fichier <em>log</em>. L’édition en LaTeX s’apparente donc au raffinement progressif d’un document final. Pour une page Web, fichiers HTML et CSS peuvent être édités dans le même espace de travail alors même qu’un moteur de rendu rafraîchit l’aperçu du résultat final à chaque caractère saisi. Simultanéité et stimulation vont alors de pair. Une dynamique d’expérimentation localisée peut se mettre en place, la souplesse du dispositif éditorial encourageant la multiplication des variations d’écriture.</p>
<p>Les SIG obéissent à la même logique, avec une particularité. Une dichotomie marque les débats entre praticiens au sujet de la nature même de la carte : est-ce une méthode ou bien une finalité ? À l’origine de ce questionnement, il faut préciser que la cartographie numérique tend plus vers le dispositif expérimental qu’éditorial. Contrairement aux autres exemples de pratiques d’édition, son informatisation a révélé un potentiel heuristique que seule l’accélération des délais de fabrication proprement dite a pu permettre. En particulier, elle s’adapte aux problématiques d’abondance informationnelle de notre époque aussi bien que le fichier encyclopédique d’Otlet s’adaptait à celles du début du XX^e^ siècle. La cartographie est une technique populaire des études digitales au sens large du terme, notamment parce qu’elle favorise une démarche exploratoire vis-à-vis des données.</p>
<p>Faut-il attribuer cette créativité stimulée à la technique elle-même ou à la finalité recherchée par l’usager ? Il serait faux de dire que la fonctionnalité seule conditionne la démarche expérimentale, de même qu’il est difficile de nier que l’aspect radicalement nouveau de cette fonction heuristique provient avant tout d’une évolution de l’outil lui-même. Considérons plutôt que les effets d’accumulation et de comparaison ne sont pas une nouveauté dans l’histoire de l’activité intellectuelle. Elle motive l’accumulation de cartes de travail et la plasticité du support est un facteur déterminant de cette possibilité nouvelle, mais les tablettes d’argile ou les tableaux noirs remplissaient peu ou prou le même usage bien avant l’invention des écrans, d’une façon archétypale. Technique et pensée n’avancent pas au même rythme ; leurs accélérations ou leurs ralentissements ne sont pas synchronisés. Les apports intellectuels de la cartographie et du caractère dynamique de l’édition numérique en général sont avant tout une manifestation de cette co-construction de la culture et de la technique, parfois saccadée mais dont la continuité est rendue manifeste par la présence de l’écriture.</p>
<h1 id="conclusion">Conclusion<a href="#conclusion" class="hanchor" ariaLabel="Anchor">&#8983;</a> </h1>
<p>En observant le fonctionnement de cette pensée outillée, on entrevoit les chemins par lesquels la créativité se renouvelle. Elle suit les évolutions des techniques les plus indissociables de la réflexion, effectuant un progrès à chaque nouvelle articulation entre l’esprit et l’écrit. La traduction pure de la pensée semble moins favoriser les élans créatifs qu’une résonance imparfaite mais dynamique, faite d’ajustements continus. Ceci explique la vitalité des processus de classification, de schématisation ou de cartographie dans notre bagage intellectuel : nous avons trouvé le moyen de tirer parti de la réduction inhérente à la plupart des formes d’écriture, en concevant des outils qui entretiennent la réflexion au lieu de l’étouffer.</p>
<p>Surtout, les logiques d’amplification et de vitesse révélées par les SIC continuent à représenter un éclairage particulièrement utile : elles montrent que certaines techniques actuelles correspondent à des manifestations accélérées de prototypes bien antérieurs ; de même qu’elles soulignent l’importance du passage à l’échelle dans l’appréhension de l’information, qu’il s’agisse de volume ou de temporalité.</p>
<p>Certaines perspectives sont toutes tracées. Il existe aujourd’hui un terrain privilégié pour le dialogue entre design et humanités par le biais des SIC, qui représentent un point de convergence théorique et pratique sur les problématiques exposées dans cet article. Il faut se confronter à ce terrain. Les nouvelles médiations de la créativité soulèvent des questions qui ne peuvent faire l’économie du code, des données et des interfaces. Inversement, l’exigence théorique n’a jamais aussi élevée : si une partie de notre appareil conceptuel a été renouvelée avec efficacité, tout est loin d’être terminé. Les deux démarches ne sont pas seulement complémentaires : elles reposent l’une sur l’autre. Avec la popularité des humanités digitales, il ne faudrait pas perdre de vue que c’est la valeur combinée du savoir-faire et de l’érudition qui fait de l’humanisme scientifique une idée si forte qu’elle en est redevenue aujourd’hui incontournable.</p>
<h1 id="références--">Références {-}<a href="#références--" class="hanchor" ariaLabel="Anchor">&#8983;</a> </h1>

      </div></div>

  
  
<div class="pagination">
    <div class="pagination__title">
        <span class="pagination__title-h"></span>
        <hr />
    </div>
    <div class="pagination__buttons">
        
        <span class="button previous">
            <a href="/notes/201809041200/">
                <span class="button__icon">←</span>
                <span class="button__text">Fragment</span>
            </a>
        </span>
        
        
        <span class="button next">
            <a href="/notes/201805240000/">
                <span class="button__text">Technologies intellectuelles</span>
                <span class="button__icon">→</span>
            </a>
        </span>
        
    </div>
</div>

  

  

</div>

  </div>

  
    <footer class="footer">
  <div class="footer__inner">
      <div class="copyright">
        <span>© Arthur Perret 2020 :: Site réalisé avec <a href="http://gohugo.io">Hugo</a> :: Thème Terminal par <a href="https://twitter.com/panr">panr</a></span>
      </div>
  </div>
</footer>

<script src="assets/main.js"></script>
<script src="assets/prism.js"></script>





  
</div>

</body>
</html>
