<!DOCTYPE html>
<html lang="fr">
<head>
  
    <title>The Architext of Biblion :: Infologie</title>
  
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">
<meta name="description" content="The architect of Babel Paul Otlet (1868-1944), a well-known figure among document scholars, dedicated his life to an ideal: peace through knowledge—building a better society by improving access to information, in the hope of reducing ignorance and fear. And while he may be regarded by some simply as an idealist, the architect of a dream, there is much to be said about his intellectual legacy.
In the later part of his life, Otlet compiled decades of publications and personal documentation into his most important books: Traité de documentation." />
<meta name="keywords" content="" />
<meta name="robots" content="noodp" />
<link rel="canonical" href="/notes/201810040000/" />




<link rel="stylesheet" href="assets/style.css">

  <link rel="stylesheet" href="assets/red.css">



<link rel="stylesheet" href="style.css">


<link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/apple-touch-icon-144-precomposed.png">

  <link rel="shortcut icon" href="img/favicon/favicon.png">



<meta name="twitter:card" content="summary" />

  <meta name="twitter:site" content="@arthurperret" />

<meta name="twitter:creator" content="Arthur Perret" />


<meta property="og:locale" content="fr" />
<meta property="og:type" content="article" />
<meta property="og:title" content="The Architext of Biblion :: Infologie">
<meta property="og:description" content="The architect of Babel Paul Otlet (1868-1944), a well-known figure among document scholars, dedicated his life to an ideal: peace through knowledge—building a better society by improving access to information, in the hope of reducing ignorance and fear. And while he may be regarded by some simply as an idealist, the architect of a dream, there is much to be said about his intellectual legacy.
In the later part of his life, Otlet compiled decades of publications and personal documentation into his most important books: Traité de documentation." />
<meta property="og:url" content="/notes/201810040000/" />
<meta property="og:site_name" content="The Architext of Biblion" />

  
    <meta property="og:image" content="img/favicon/favicon.png">
  

<meta property="og:image:width" content="2048">
<meta property="og:image:height" content="1024">


  <meta property="article:published_time" content="2018-10-04 00:00:00 &#43;0000 UTC" />












</head>
<body class="">


<div class="container center headings--one-size">

  <header class="header">
  <div class="header__inner">
    <div class="header__logo">
      <a href="/">
  <div class="logo">
    INFOLOGIE
  </div>
</a>

    </div>
    <div class="menu-trigger">menu</div>
  </div>
  
    <nav class="menu">
  <ul class="menu__inner menu__inner--desktop">
    
      
        
          <li><a href="/a-propos">À propos</a></li>
        
      
      
    

    
  </ul>

  <ul class="menu__inner menu__inner--mobile">
    
      
        <li><a href="/a-propos">À propos</a></li>
      
    
    
  </ul>
</nav>

  
</header>


  <div class="content">
    
<div class="post">
  <h1 class="post-title">
    <a href="/notes/201810040000/">The Architext of Biblion</a></h1>
  <div class="post-meta">
    
      <span class="post-date">
        2018-10-04 
      </span>
    
    
    <span class="post-author">::
      Arthur Perret
    </span>
    
  </div>

  

  

  

  <div class="post-content"><div>
        <h1 id="the-architect-of-babel">The architect of Babel<a href="#the-architect-of-babel" class="hanchor" ariaLabel="Anchor">&#8983;</a> </h1>
<p>Paul Otlet (1868-1944), a well-known figure among document scholars, dedicated his life to an ideal: peace through knowledge—building a better society by improving access to information, in the hope of reducing ignorance and fear. And while he may be regarded by some simply as an idealist, the architect of a dream, there is much to be said about his intellectual legacy.</p>
<p>In the later part of his life, Otlet compiled decades of publications and personal documentation into his most important books: <em>Traité de documentation. Le livre sur le livre</em> (1934) and <em>Monde, essai d’universalisme</em> (1935). The <em>Traité</em> is widely considered to be the first manual of documentation.</p>
<p>Among many insights, it introduces the notion of <em>biblion</em>—a unit of information around which Otlet designs a framework for document theory [@robert2015]. It is a fairly ambiguous term, referring to both media and meaning, the physical object (document or book) and the information it carries. This is because, in Otlet’s view, information can take as many shapes as there are media to inscribe, far beyond the limited range of the book. A document is simply information recorded for transmission. He therefore uses a unit as a way to <em>handle</em> information on multiple levels: theoretically, because the idea of information beyond media is quite abstract; mechanically, as documents are transformed into index cards which are the units of a file system; mathematically, as information is encoded into a decimal classification.</p>
<p>The <em>Traité</em> contains a great number of fascinating statements, specifically in the way it echoes our own preoccupation with infobesity and misinformation. It had a role in the advent of documentation as a field of professional practice and research, with lasting impact on document theoreticians. It is also a daunting read: it contains 350,000 words, set in a 2-column layout over 431 pages of wide in-4°; it has only ever been reprinted twice, in facsimile editions (in 1989 and 2015); the style is very much encyclopedic, with an obsession for systematic description which has been described as tedious at times [@rayward2012]. Thus the “Bible of documentation” metaphor comes to mind.</p>
<p>One of Otlet’s projects was to build a World City, with information pathways closing the distances between men, and knowledge as its beating heart. Though it never came to be, there are echoes of this Babelian enterprise in our digital age. Otlet’s written work sheds some light on contemporary issues related to information; it also contributes to an epistemology of information science rooted in document theory.</p>
<p>In this paper, we focus on the <em>Traité</em> itself, specifically the way it can illustrate an intellectual lineage between the analog and digital environment, both conceptually and empirically.</p>
<h1 id="from-biblion-to-architext">From biblion to architext<a href="#from-biblion-to-architext" class="hanchor" ariaLabel="Anchor">&#8983;</a> </h1>
<p>In section 243 of his <em>Traité</em>, Otlet describes various “substitutes of the book” which, because of the technological advances of his time, represent a growing body of new documents: discs, films, performances, objects used as evidence and many more. This notion sketches a very open definition of the document, which was expanded even further by Suzanne Briet and Robert Pagès [@buckland2017], becoming almost overwhelming in its scope.</p>
<p>The categorization of these “substitutes” is made possible by the biblion: a concept which lays the foundation for an atomistic view of information. The word itself shares the ambiguity of “book” or “document” in the context of Otlet’s writing, where they are polysemic, often substituted for one another, and can designate different things depending on which part of the <em>Traité</em> they appear in. He writes:</p>
<blockquote>
<p>“Until an agreement be made on unified terminology, we will use indifferently the terms formed of the following four radicals, two Greek, two Latin, giving them by convention an equivalent meaning: 1° biblion, 2° grapho (gram grammata), 3° liber, 4° documentum.” [@otlet1934, p. 12]</p>
</blockquote>
<p>Consequently, he defines biblion as either:</p>
<blockquote>
<p>“a generic term for all species [of documents]” (p. 43);<br>
“the intellectual, abstract unit [of information]” (p. 43);<br>
“writing and text” (p. 372), “writings” (p. 373).</p>
</blockquote>
<p>Therefore biblion means document but also the information carried by a document, regardless of its specific shape. With this concept, Otlet theorized how information could take a more flexible form, far beyond the book.</p>
<p>The biblion is closely tied to writing and could be regarded as meaning data, for it opens a path to conceiving texts as databases. Indeed, with computing, we are moving from a document paradigm to another, loosely defined as data-centric, which is often presented as entirely new approach. However, while digital objects do vary in shape, dimension and granularity, they simply raise the same issue as Otlet’s substitutes, Briet’s antelope or Pagès’s gorilla—that of a conceptual framework which would tie them together while being coherent with practical implications.</p>
<p>By defining documents in such manner, Otlet foreshadowed a non-linear read/write system, hypertext, but we will use another term, which provides high-level description: architext. The concept originated in literary studies, where architextuality refers to texts as part of genres [@genette1992]. The word carried over to information science, where it was interpreted as the architecture which marks out text and governs its enunciation [@jeanneret1999]. Using the word <em>text</em> to designate a literary object as a whole semantic field  [@treharne2009], the architext can be seen as:</p>
<ul>
<li>everything which is not <em>text</em> but related to it;</li>
<li>a form of writing that expresses <em>text</em>.</li>
</ul>
<p>This concept is especially relevant in a digital environment, as it helps us understand how computing implements the delegation of some architectural function to writing itself, and what we can derive from that.</p>
<p>At a simple level, the architext is the markup that allows text to be structured and rendered in a specific way: it is a way of encoding text, with instructions made of words and delimiters, such as the iconic <code>&lt;/&gt;</code> tags found in all SGML-derived languages (e.g., XML or HTML). At a higher level, the architext enables hyperdocuments by expressing links between texts: from a single URI to entire programming libraries, hypertextuality connects different types of documents with various levels of granularity—all this through markup.</p>
<p>It should be noted that architext does not mean metadata. In their most simple form, they seem to overlap: a title and date at the top of a sheet of paper are metadata and their documentary functions do contribute to the expression of text (stabilizing information, allowing for quicker reference, constituting evidence). However, a digital architext is mostly made of structural components which carry no information at all: intrinsically meaningless elements used to apply formatting (such as <code>div</code> and <code>span</code> tags in HTML); layout instructions written in code (such as Javascript); anchors allowing for navigation; etc. The common aspect and the very bones of it all are non-alphabetical characters, either borrowed from punctuation or invented along the developments of typography—a veritable <em>scripturation</em> [@laufer1986] which warrants dedicated research of its own.</p>
<p>This “hyperdocumentation” is at the core of the <em>Traité</em>’s most difficult excerpts, in which Otlet anticipates a paradigm we are now living in (the Internet), while also describing things we cannot readily grasp—sometimes verging on the paranormal. Leaving that last part aside, we will focus here on how this framework of concepts can be applied in a very practical approach.</p>
<h1 id="an-experiment-in-digital-hermeneutics">An experiment in digital hermeneutics<a href="#an-experiment-in-digital-hermeneutics" class="hanchor" ariaLabel="Anchor">&#8983;</a> </h1>
<blockquote>
<p>“The <em>Traité de documentation</em> contains two sections, unequal in size. The longest one is a systematic description of the book and the document . . . The shortest section is dedicated to bibliology and it is of the utmost importance for this field of study.” [@estivals1987, p. 13]</p>
</blockquote>
<p>This is one example of a comment on Otlet’s <em>Traité</em> that we can come across when scanning the literature in search of useful companion pieces to the book itself. It makes three statements, respectively about structure, content and significance. They could be verified at a glance using the table of contents as well as more in-depth literature on bibliology [@estivals1993, p. 30-65], and then be made clearer through selective reading of the <em>Traité</em>. This would be the classic, qualitative process of text analysis.</p>
<p>In this article, our goal is to illustrate the benefits of a quantitative approach. By cross-referencing simple structural information with text statistics and classification, we are able to reach a similar level of description. More importantly, it brings up observations that could not be made before, allowing us to formulate hypotheses from a different angle. As such, we aim to highlight the heuristic potential of exploring text as data.</p>
<p>We devised a small experiment which relies on the architext-biblion tandem. The former enables the latter: markup allows us to extract the intellectual content inside a digital document, as well as create distinct units of information inside it. This opens new possibilities in terms of processing. The flexibility of digital text means we can test the heuristic potential and hermeneutical value of several text structures and representations (e.g., list, table, graph).</p>
<p>We chose two complementary approaches:</p>
<ol>
<li>transcribe the table of contents of the <em>Traité</em> as tabular data, then build structural representations;</li>
<li>encode the entire content as raw text, then apply standard corpus analysis techniques (lexicometry).</li>
</ol>
<p>A combination of 3 documents were used: the 2015 reprint of the <em>Traité</em>, the full text <a href="https://fr.wikisource.org/wiki/Trait%C3%A9_de_documentation">from Wikisource</a> and the EPUB version exported from the full text. The corpus file was formatted for processing with Iramuteq, with variables encoding the 6 main sections of the book. The table of contents was revised and extended manually to include six levels of depth from a partially automated extraction based on regular expressions, then processed with RAWGraphs.</p>
<h1 id="hierarchical-data-visualization-and-lexicometry">Hierarchical data visualization and lexicometry<a href="#hierarchical-data-visualization-and-lexicometry" class="hanchor" ariaLabel="Anchor">&#8983;</a> </h1>
<p>Schematization is fundamental to Otlet’s approach. In particular, his archives contain many representations of networks as well as radiant and arborescent structures. The visualization methods we applied to the structural data draws from this focus on circular and structural imagery.</p>
<p>The circular dendrogram is a hierarchical tree arranged in a circle. Here, each node represents an entry in the table of contents, with links corresponding to ancestry and filiation. The node at the center of the figure represents the book. Nodes are ordered clockwise according to the numbering of the book.</p>
<p><img src="../img/the-architext-of-biblion-circular-dendrogram-13.png" alt="Circular dendrograms at level-1 (left) and level-3 depth (right)"></p>
<p><img src="../img/the-architext-of-biblion-circular-dendrogram-6.png" alt="Circular dendrogram at level-6 depth"></p>
<p>Figure 1 shows the first level of the hierarchy, with a node representing the book at the center, and each of the six main sections placed clockwise according to their number (<em>0. Fundamenta</em>; <em>1. La Bibliologie ou Documentologie</em>; <em>2. Le livre et le document</em>; <em>3. Le livre et le document. Unités ou Ensembles</em>; <em>4. Organisation rationnelle du Livre et du Document</em>; <em>5. Synthèse bibliologique</em>). Going deeper into the table of contents, the dendrogram shows an uneven distribution of subsections across the book, with part 1 and part 2 displaying many more ramifications than part 0. At depth level 6, the complexity of the structure is made quite apparent.</p>
<p><img src="../img/the-architext-of-biblion-treemap.png" alt="Treemap showing the 6 main sections and their immediate subsections"></p>
<p>To gain a sense of the sections’ relative proportions, we apply another method, the treemap. Here, each block represents a level-2 entry in the table of contents. Blocks are grouped by sections, with slightly larger spacing between groups to better distinguish the 6 sections. We then input the word count for each entry, therefore resizing the blocks to match their relative proportions. The treemap shows a striking difference in volume across sections, with part 2 (<em>Le livre et le document</em>) clearly representing the biggest segment of the book.</p>
<p><img src="../img/the-architext-of-biblion-treemap-weighted.png" alt="Proportional treemap (adjusted with word count)"></p>
<p><img src="../img/the-architext-of-biblion-nuage.png" alt="Most frequent words encountered in the book"></p>
<p>In order to use our hierarchical data in a meaningful way, we move on to an analysis of the full text. The first and most simple method we apply is a word cloud, which represents word frequency across the <em>Traité</em>. The title of the book is <em>Traité de documentation</em> but the subtitle is <em>Le livre sur le livre</em>. Given how interchangeable the words “document” and “book” are in Otlet’s writings, it could come off as a surprise that the latter dominates the numbers so clearly. It goes to show how important it is in Otlet’s argumentation.</p>
<p>By essence, a word cloud suggests which ideas are at the core of a text, with further verifications required to make that claim with absolute certainty. The similarities analysis can give us a first glimpse at the lexical repartition, informing us on the relationships between the most frequent words in context.</p>
<p><img src="../img/the-architext-of-biblion-similarities.png" alt="Similarities analysis showing relative homogeneity across the book"></p>
<p>It is a somewhat difficult representation to work with. Readability and size are inversely proportional, which means that the surface of a work-in-progress is usually significantly bigger than that of the figure shared in a paper. Nevertheless, the flower-like distribution is a good indicator of homogeneity in a corpus; here, it confirms that the word “book” is not simply the most frequent word in the text but also the most central idea in it. “Documentation” stands out, as it not directly related to the word “book”: it is linked with the organizational aspects of Otlet’s work, with international cooperation appearing as a structuring parameter in the use and perhaps the definition of the word.</p>
<p>The bulk of the lexicometry depends on the classification and subsequent correspondence analysis. A global snapshot of the lexical profile is sufficient to glimpse the contents of the book: with 5 classes, we can distinguish the bibliographical description, the organization of knowledge and the matters of science. However, we wish for a more accurate profile, which is why we move on to a hierarchical descending classification [@reinert1983]. We settle empirically for a setting which yields the most meaningful distribution, resulting in 12 lexical classes. Figure 7 shows the result; word size is not correlated to frequency but specificity.</p>
<p><img src="../img/the-architext-of-biblion-classesafc.png" alt="Proposed lexical classification"></p>
<p>Since the division of the <em>Traité</em> in parts was encoded as variables, we can plot them to obtain their lexical repartition. Figure 8 shows that, as far as lexical classes are concerned, there is a clear separation between two sets of book parts: [0, 1, 4, 5] and [2, 3].</p>
<p><img src="../img/the-architext-of-biblion-classesparts.png" alt="Book parts’ lexical repartition"></p>
<p>How do we link parts and classes? This is where statistics are of great interest: since they are not readily available in a qualitative approach, they bring up interesting observations that may have come up much later otherwise, if at all. We look in particular at frequency, which is a simple count that can also be calculated relatively, and specificity, which results from a chi-square test.</p>
<p>The following table indicates whether the specificity of book parts to each class is positive (+) or negative (-). We judged the specificity score based on a significance criteria, aiming to highlight true positives: a low positive score in a short book part was not deemed significant and therefore treated as a negative. A brief but necessarily reductive description of each class’ dominant aspects is provided, to help with the data’s legibility.</p>
<table>
<thead>
<tr>
<th align="right">Class</th>
<th align="center">Part 0</th>
<th align="center">Part 1</th>
<th align="center">Part 2</th>
<th align="center">Part 3</th>
<th align="center">Part 4</th>
<th align="center">Part 5</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td align="right">1</td>
<td align="center">-</td>
<td align="center">-</td>
<td align="center">+</td>
<td align="center">+</td>
<td align="center">-</td>
<td align="center">+</td>
<td align="left">humanities &amp; spirituality</td>
</tr>
<tr>
<td align="right">2</td>
<td align="center">+</td>
<td align="center">-</td>
<td align="center">-</td>
<td align="center">-</td>
<td align="center">+</td>
<td align="center">-</td>
<td align="left">documentation methods</td>
</tr>
<tr>
<td align="right">3</td>
<td align="center">-</td>
<td align="center">-</td>
<td align="center">+</td>
<td align="center">-</td>
<td align="center">-</td>
<td align="center">-</td>
<td align="left">bibliographical information</td>
</tr>
<tr>
<td align="right">4</td>
<td align="center">+</td>
<td align="center">-</td>
<td align="center">-</td>
<td align="center">-</td>
<td align="center">+</td>
<td align="center">-</td>
<td align="left">organization, society &amp; politics</td>
</tr>
<tr>
<td align="right">5</td>
<td align="center">-</td>
<td align="center">-</td>
<td align="center">-</td>
<td align="center">-</td>
<td align="center">+</td>
<td align="center">-</td>
<td align="left">knowledge institutions &amp; communities</td>
</tr>
<tr>
<td align="right">6</td>
<td align="center">-</td>
<td align="center">-</td>
<td align="center">+</td>
<td align="center">-</td>
<td align="center">-</td>
<td align="center">-</td>
<td align="left">material bibliography</td>
</tr>
<tr>
<td align="right">7</td>
<td align="center">-</td>
<td align="center">-</td>
<td align="center">+</td>
<td align="center">-</td>
<td align="center">-</td>
<td align="center">-</td>
<td align="left">media technologies</td>
</tr>
<tr>
<td align="right">8</td>
<td align="center">-</td>
<td align="center">+</td>
<td align="center">-</td>
<td align="center">-</td>
<td align="center">-</td>
<td align="center">+</td>
<td align="left">epistemology</td>
</tr>
<tr>
<td align="right">9</td>
<td align="center">+</td>
<td align="center">+</td>
<td align="center">-</td>
<td align="center">-</td>
<td align="center">+</td>
<td align="center">+</td>
<td align="left">document science</td>
</tr>
<tr>
<td align="right">10</td>
<td align="center">-</td>
<td align="center">-</td>
<td align="center">+</td>
<td align="center">-</td>
<td align="center">-</td>
<td align="center">-</td>
<td align="left">publishing &amp; economy</td>
</tr>
<tr>
<td align="right">11</td>
<td align="center">-</td>
<td align="center">+</td>
<td align="center">-</td>
<td align="center">-</td>
<td align="center">-</td>
<td align="center">+</td>
<td align="left">sciences</td>
</tr>
<tr>
<td align="right">12</td>
<td align="center">-</td>
<td align="center">-</td>
<td align="center">+</td>
<td align="center">+</td>
<td align="center">-</td>
<td align="center">-</td>
<td align="left">history &amp; historiography</td>
</tr>
</tbody>
</table>
<p>: Book part specificity depending on class</p>
<p>The specificity score can also be used to look at smaller units of text, namely word forms, as seen in Table 2.</p>
<table>
<thead>
<tr>
<th align="left">Form</th>
<th align="right">Part 0</th>
<th align="right">Part 1</th>
<th align="right">Part 2</th>
<th align="right">Part 3</th>
<th align="right">Part 4</th>
<th align="right">Part 5</th>
<th align="right">Freq.</th>
</tr>
</thead>
<tbody>
<tr>
<td align="left">livre</td>
<td align="right">-0.3</td>
<td align="right">22.8</td>
<td align="right">-9.2</td>
<td align="right">-0.4</td>
<td align="right">-9.5</td>
<td align="right">20.4</td>
<td align="right">2048</td>
</tr>
<tr>
<td align="left">grand</td>
<td align="right">-0.3</td>
<td align="right">-1.3</td>
<td align="right">1.7</td>
<td align="right">0.3</td>
<td align="right">-2.0</td>
<td align="right">1.7</td>
<td align="right">829</td>
</tr>
<tr>
<td align="left">bibliothèque</td>
<td align="right">1.5</td>
<td align="right">-9.0</td>
<td align="right">-0.8</td>
<td align="right">-4.2</td>
<td align="right">15.2</td>
<td align="right">-6.8</td>
<td align="right">781</td>
</tr>
<tr>
<td align="left">science</td>
<td align="right">0.4</td>
<td align="right">65.6</td>
<td align="right">-45.9</td>
<td align="right">0.8</td>
<td align="right">0.5</td>
<td align="right">3.4</td>
<td align="right">774</td>
</tr>
<tr>
<td align="left">document</td>
<td align="right">6.6</td>
<td align="right">4.4</td>
<td align="right">-24.1</td>
<td align="right">-2.1</td>
<td align="right">15.5</td>
<td align="right">2.3</td>
<td align="right">638</td>
</tr>
</tbody>
</table>
<p>: Word form specificity according to book part, with frequency</p>
<h1 id="discussion">Discussion<a href="#discussion" class="hanchor" ariaLabel="Anchor">&#8983;</a> </h1>
<p>Our results show indeed that the <em>Traité</em> is a two-legged piece of work, if somewhat lopsided, with an overgrown bibliographic section bookended by shorter but dense epistemological work.</p>
<p>Figure 8 and Table 1 all but confirm this division. However, the data also underlines the finer dynamics of the first set [0, 1, 4, 5]. Part 0 and 5, being the introduction and conclusion, present their own variation on a common lexical profile; this reflects the necessary mix of vocabulary used in such context and is not surprising. Much more interesting is the difference between the other two, with part 1 seemingly containing most of the epistemological effort, while part 4 moves the need for a document science to its systematic application, with a sense of urgency brought by the technical, social and political challenges of Otlet’s time. There is a common theme, but it is weaved differently.</p>
<p>This brings up the question of which thread was pulled. We know that in the following decades, scientific bibliology was almost abandoned, save for the occasional remembrance, while documentation thrived as a new area of practice. It calls to question whether the contents of part 4 were simply deemed more achievable by Otlet’s readers, as opposed to the daunting prospects of inventing a new science, even though they were so closely linked. Perhaps a greater clarity of purpose played a part in consolidating documentation, as shown by the contributions of Suzanne Briet and her students (not least among them Robert Pagès). Bibliology, on the other hand, has remained a minor subject—although for reasons which are not limited to the <em>Traité de documentation</em>.</p>
<p>The data presented in Table 2 brings up another observation. The word frequency values for “book” and “document” are very high; they are at the heart of the <em>Traité</em>, as illustrated by the word cloud on Figure 5. Because of the sheer amount of times they occur, and taking into account the size of each book part, their low specificity to [2, 3] comes as a bit of a surprise. It is as if Otlet extracted the words from material bibliography and tied them irrevocably to a singular idea, blurring the lines between the terms. However, this ambiguity is not accidental: we have seen that he actually argues for the indifferent use of <em>biblion</em>, <em>graphein</em> or <em>gramma</em>, <em>liber</em> or <em>documentum</em> to form concepts until a consensus is reached.</p>
<p>Can we say that this consensus has indeed been reached? What about the importance that <em>data</em> has taken nowadays? Again, this can be tied to the question of Otlet’s epistemological legacy. We know that the <em>Traité</em> belongs to a certain lineage, that it represents the culmination of a life’s work for Otlet but also some of his colleagues and of course their predecessors working on bibliology; we also know how the book was received and the discreet influence it had in the following years. However, we know less about the extension of this lineage into the end of the 20th century and the beginning of the 21st. New approaches have been developed to adapt to a seemingly new information paradigm; the fate and relevance of Otlet’s conceptual choices could be studied, perhaps with a mix of qualitative and quantitative methods.</p>
<p>Leaving these questions aside for another, more expansive study, there are two final considerations to be made.</p>
<p>Firstly, we now have many powerful tools that support different hermeneutical approaches to documents in general and text in particular. They sometimes yield quick results, in which case they should be used with twice as much caution, to avoid snowballing into absurd conclusions. As a general rule, these tools not only benefit from being articulated with a coherent theoretical framework, they require it to make any sort of significant observation, as small as it may be.</p>
<p>Here, we hope to have demonstrated the interest that lies in a science of writing that informs both concept and experiment. The goal was to show what information quantitative methods bring to the table and how they feed back into a reflection on the text, its interpretation, its significance. Lexicometry is especially interesting for the study of theories: it provides data and representations for key concepts from a corpus, informing us on the correlations between structure and meaning.</p>
<p>Secondly, visual methods should not be seen as a simple means to an end, a technique used to produce a support for communication. They constitute a proper methodology as well, providing a way to test assumptions and explore sources. This is especially apparent when working with real-time rendering, which stimulates experimental approaches. Of course, this does not exclude the matter of output and exports, as the figures in this paper show. It simply means to reiterate that all forms of writing play a complex part in the way we think and work—something which Otlet probably had in mind when he included schematization in the constitutive elements of bibliology, the science of writing.</p>
<h1 id="references--">References {-}<a href="#references--" class="hanchor" ariaLabel="Anchor">&#8983;</a> </h1>

      </div></div>

  
  
<div class="pagination">
    <div class="pagination__title">
        <span class="pagination__title-h"></span>
        <hr />
    </div>
    <div class="pagination__buttons">
        
        <span class="button previous">
            <a href="/notes/201810061200/">
                <span class="button__icon">←</span>
                <span class="button__text">Architexte Treharne</span>
            </a>
        </span>
        
        
        <span class="button next">
            <a href="/notes/201809291200/">
                <span class="button__text">Bibliologie</span>
                <span class="button__icon">→</span>
            </a>
        </span>
        
    </div>
</div>

  

  

</div>

  </div>

  
    <footer class="footer">
  <div class="footer__inner">
      <div class="copyright">
        <span>© Arthur Perret 2020 :: Site réalisé avec <a href="http://gohugo.io">Hugo</a> :: Thème Terminal par <a href="https://twitter.com/panr">panr</a></span>
      </div>
  </div>
</footer>

<script src="assets/main.js"></script>
<script src="assets/prism.js"></script>





  
</div>

</body>
</html>
