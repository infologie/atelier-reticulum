<!DOCTYPE html>
<html lang="fr">
<head>
  
    <title>Documentarité et données, instrumentation d’un concept :: Infologie</title>
  
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">
<meta name="description" content="Introduction L&amp;rsquo;essor du Web avait occasionné en France une vaste réflexion sur l&amp;rsquo;articulation entre ancien et nouveau régime documentaire, notamment autour de la notion de « redocumentarisation » [@pedauque2007] et de ses critiques [@courbieres2008]. Une nouvelle période de questionnements s&amp;rsquo;est ouverte depuis, polarisée par la notion de données, laquelle interroge comme précédemment les fondamentaux de la gestion de connaissances, ainsi que ceux des sciences de l&amp;rsquo;information et de la communication (SIC) de façon plus globale." />
<meta name="keywords" content="" />
<meta name="robots" content="noodp" />
<link rel="canonical" href="/notes/201910090000/" />




<link rel="stylesheet" href="assets/style.css">

  <link rel="stylesheet" href="assets/red.css">



<link rel="stylesheet" href="style.css">


<link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/apple-touch-icon-144-precomposed.png">

  <link rel="shortcut icon" href="img/favicon/favicon.png">



<meta name="twitter:card" content="summary" />

  <meta name="twitter:site" content="@arthurperret" />

<meta name="twitter:creator" content="Arthur Perret, Olivier Le Deuff" />


<meta property="og:locale" content="fr" />
<meta property="og:type" content="article" />
<meta property="og:title" content="Documentarité et données, instrumentation d’un concept :: Infologie">
<meta property="og:description" content="Introduction L&amp;rsquo;essor du Web avait occasionné en France une vaste réflexion sur l&amp;rsquo;articulation entre ancien et nouveau régime documentaire, notamment autour de la notion de « redocumentarisation » [@pedauque2007] et de ses critiques [@courbieres2008]. Une nouvelle période de questionnements s&amp;rsquo;est ouverte depuis, polarisée par la notion de données, laquelle interroge comme précédemment les fondamentaux de la gestion de connaissances, ainsi que ceux des sciences de l&amp;rsquo;information et de la communication (SIC) de façon plus globale." />
<meta property="og:url" content="/notes/201910090000/" />
<meta property="og:site_name" content="Documentarité et données, instrumentation d’un concept" />

  
    <meta property="og:image" content="img/favicon/favicon.png">
  

<meta property="og:image:width" content="2048">
<meta property="og:image:height" content="1024">


  <meta property="article:published_time" content="2019-10-09 00:00:00 &#43;0000 UTC" />












</head>
<body class="">


<div class="container center headings--one-size">

  <header class="header">
  <div class="header__inner">
    <div class="header__logo">
      <a href="/">
  <div class="logo">
    INFOLOGIE
  </div>
</a>

    </div>
    <div class="menu-trigger">menu</div>
  </div>
  
    <nav class="menu">
  <ul class="menu__inner menu__inner--desktop">
    
      
        
          <li><a href="/a-propos">À propos</a></li>
        
      
      
    

    
  </ul>

  <ul class="menu__inner menu__inner--mobile">
    
      
        <li><a href="/a-propos">À propos</a></li>
      
    
    
  </ul>
</nav>

  
</header>


  <div class="content">
    
<div class="post">
  <h1 class="post-title">
    <a href="/notes/201910090000/">Documentarité et données, instrumentation d’un concept</a></h1>
  <div class="post-meta">
    
      <span class="post-date">
        2019-10-09 
      </span>
    
    
    <span class="post-author">::
      Arthur Perret, Olivier Le Deuff
    </span>
    
  </div>

  

  

  

  <div class="post-content"><div>
        <h1 id="introduction">Introduction<a href="#introduction" class="hanchor" ariaLabel="Anchor">&#8983;</a> </h1>
<p>L&rsquo;essor du Web avait occasionné en France une vaste réflexion sur l&rsquo;articulation entre ancien et nouveau régime documentaire, notamment autour de la notion de « redocumentarisation » [@pedauque2007] et de ses critiques [@courbieres2008]. Une nouvelle période de questionnements s&rsquo;est ouverte depuis, polarisée par la notion de données, laquelle interroge comme précédemment les fondamentaux de la gestion de connaissances, ainsi que ceux des sciences de l&rsquo;information et de la communication (SIC) de façon plus globale. De la même façon que se dessinent les contours d&rsquo;un imprimé post-numérique, au sens d&rsquo;un paradigme commun aux deux supports, le document résiste épistémologiquement à une certaine forme de discours techno-prophétique qui aurait voulu lui substituer la donnée sans autre forme de procès. C&rsquo;est ainsi qu&rsquo;après avoir longuement interrogé ce que devient le document au 21^e^ siècle, un nouveau questionnement émerge : qu&rsquo;est-ce qui est susceptible de « faire document » dans les données et les mégadonnées ? En réponse à cela, nous proposons d&rsquo;examiner le concept de documentarité, dont les origines sont multidisciplinaires. Il s&rsquo;agit de situer ce concept vis-à-vis de la recherche sur les propriétés de la notion de document ainsi que sur l&rsquo;épistémologie des données, puis d&rsquo;examiner son instrumentation.</p>
<h1 id="documentation-et-agentivité">Documentation et agentivité<a href="#documentation-et-agentivité" class="hanchor" ariaLabel="Anchor">&#8983;</a> </h1>
<p>De nombreuses études sur la documentation menées ces dernières années se basent sur une redécouverte de ses pionniers européens, avec notamment les publications essentielles de Paul Otlet [-@otlet1934], Suzanne Briet [-@briet1951] ou encore Robert Pagès [-@pages1948]. Il s&rsquo;agit en particulier d&rsquo;un renouveau théorique important pour la bibliothéconomie anglo-saxonne. Les concepts fondamentaux de la documentation sont réinterrogés à l&rsquo;aune de problématiques nouvelles, dans une perspective double : éclairer des phénomènes contemporains et en renouveler la théorie. Ces phénomènes découlent en partie du développement de l&rsquo;informatique. Confrontés à de nouvelles terminologies et typologies d&rsquo;objets, les chercheurs en documentation consacrent une attention particulière à l&rsquo;élaboration de cadres conceptuels qui permettent d&rsquo;en étudier les propriétés.</p>
<p>La question la plus travaillée reste celle du document et de ce qui l&rsquo;asseoit ontologiquement en tant que concept. Une propriété ayant fait l&rsquo;objet de travaux importants est la <em>documentalité</em>. Bernd Frohmann [-@frohmann2012] définit la documentalité comme une capacité à générer des traces. Maurizio Ferraris [-@ferraris2013] a quant a lui proposé une théorie de la documentalité définie comme l&rsquo;inscription des actes sociaux sous forme de documents. Ainsi que le relève Claire Scopsi [-@scopsi2018], les deux approches se rapportent à l&rsquo;agentivité du document. Toutefois, Frohmann s&rsquo;inscrit dans une critique de l&rsquo;anthropocentrisme qui donne à sa définition une plus grande généri-cité. Il applique à la documentation l&rsquo;argument de Bruno Latour suivant lequel la division entre les propriétés primaires (intrinsèques) et secondaires (ressenties) d&rsquo;un objet doit être remise en cause. Frohmann montre notamment qu&rsquo;une trace ne dépend pas de son enregistrement par l&rsquo;homme et généralise l&rsquo;affirmation de Suzanne Briet suivant laquelle une antilope dans un zoo constitue un document :</p>
<blockquote>
<p>« L&rsquo;argument sous-jacent de l’antilope de Briet est que quelque chose devient un document en vertu de ses arrangements avec d’autres choses, et non d’une forme privilégiée de ces arrangements, telle que sa fonction de preuve ». [trad. libre de @frohmann2012, 173]</p>
</blockquote>
<p>On retrouve dans cette approche une logique hyperdocumentaire qui ne procède pas de l&rsquo;hypertexte mais de la vision de Paul Otlet sur l&rsquo;évolution possible de la documentation. Otlet suggère en effet qu&rsquo;une fusion document-instrument permettrait une réticularisation du réel avec la connaissance. Dans cette perspective, les propriétés intrinsèques des choses et leur perception par nos sens se fondent en une « Hyper-Documentation » ainsi qu&rsquo;une « Hyper-Intelligence » contigües [@otlet1934, p. 429].</p>
<p>Ainsi que le montre Michael Buckland [-@buckland2017], de telles possibilités avaient été envisagées par Robert Pagès, contemporain et élève de Suzanne Briet dont il a peut-être inspiré l&rsquo;exemple de l&rsquo;antilope. Pagès [-@pages1948] utilise un gorille pour illustrer la notion d&rsquo;autodocument. Ron Day [-@day2018] mobilise cette idée pour décrire une auto-documentalité qui s&rsquo;inscrit selon nous dans le raisonnement de Frohmann. Day rappelle que la documentation traditionnelle envisage peu l&rsquo;agentivité du document :</p>
<blockquote>
<p>« Contrairement à notre façon de penser, tous les documents, qu’ils soient iconiques ou figuratifs, sont utilisés de manière pragmatique sur le mode de l&rsquo;index […] La documentalité est prescriptive, la documentation est descriptive ». [trad. libre de @day2018, p. 8]</p>
</blockquote>
<p>La documentalité, entendue comme « force documentaire », peut donc être formulée ainsi : il s&rsquo;agit de <em>ce que fait</em> un objet du point de vue documentaire, le mot document ayant alors une acception plus large que dans le sens traditionnel de document imprimé ou numérique.</p>
<h1 id="ce-qui-fait-document">Ce qui fait document<a href="#ce-qui-fait-document" class="hanchor" ariaLabel="Anchor">&#8983;</a> </h1>
<p>Nous souhaitons discuter ici une autre propriété fondamentale, dans une formulation proche et pourtant radicalement différente : <em>ce qui fait</em> document, et que nous appellerons ici documentarité. La documentarité est ce qui fait document <em>aux yeux de</em> — elle explicite le caractère central de la dimension interprétative dans ce qui constitue, lors sa réception, un document. Il s&rsquo;agit d&rsquo;une certaine façon de réintroduire la question de la subjectivité, tout en prenant en compte celle de l&rsquo;agentivité. Notre démarche rejoint celle exprimée par Ron Day, qui a consacré un ouvrage à la documentarité comme philosophie de l&rsquo;évidence :</p>
<blockquote>
<p>« Au lieu d&rsquo;aborder l&rsquo;information sous un angle ontologique (“qu&rsquo;est-ce que l&rsquo;information ?”), ce livre aborde l&rsquo;ontologie sous l&rsquo;angle informationnel, en examinant comment quelque chose devient évident et peut être pris comme preuve de ce qui est ». [trad. libre de @day2019, 1]</p>
</blockquote>
<p>Selon lui, on peut distinguer historiquement une documentarité « forte » basée sur des catégories de jugement a priori et une documentarité « faible » basée sur l&rsquo;inscription socio-culturelle. Day emprunte à l&rsquo;ontologie une autre distinction qui vient recouper et approfondir la première : celle entre les dispositions (c&rsquo;est-à-dire les pouvoirs d&rsquo;expressions intrinsèques) et les affordances (soit l&rsquo;influence de l&rsquo;environnement). L&rsquo;ensemble sous-tend ce qu&rsquo;il nomme des « technologies du jugement ».</p>
<p>En SIC, un « principe de documentarité » a été proposé par Stéphane Crozat :</p>
<blockquote>
<p>« La documentarité est une mesure de ce que permet un contenu en terme de contrat de lecture du point de vue de ses propriétés documentaires ». [@crozat2016; -@crozat2019]</p>
</blockquote>
<p>Partant du constat que l’information est nécessairement incarnée par des objets (il n&rsquo;y a pas d’information sans support), Crozat soulève le problème suivant : dans un environnement numérique, la documentarité de ces objets — c’est-à-dire la façon dont ils font sens — n’est pas évidente. Plus précisément, leur documentarisation n’est pas explicite ou bien se déplace au niveau de la réception par le lecteur.</p>
<p>L&rsquo;utilisation du mot documentarité dans un contexte théorique a une existence antérieure et extérieure aux SIC, dans un texte d&rsquo;André Gaudreault et Philippe Marion [-@gaudreault1994] sur le documentaire en tant que genre cinématographique. Les auteurs y questionnent ce qui peut « faire document » (p. 13) ; selon eux, le récit documentaire s&rsquo;oppose au récit de fiction, et c&rsquo;est à sa lecture (à son visionnage) que se jauge sa « documentarité » (p. 17). Les termes clés de notre problématique sont donc bien présents mais l&rsquo;intérêt du texte dépasse une simple proximité de vocabulaire. En effet, les auteurs mobilisent ces termes pour faire une analyse de la vérité comme rapport au réel qui fait écho aux problématiques que nous avons soulevées. Selon eux, la documentarisation n&rsquo;est pas qu&rsquo;un simple mécanisme d&rsquo;enregistrement, puisqu&rsquo;elle produit un « effet de trace […] empreinte non pas du réel mais, plutôt, du sujet graphiateur, sujet-énonciateur » (p. 17). Elle constitue donc un processus d&rsquo;éditorialisation par lequel on produit une vérité qui tend vers le réel. La documentarisation diffère ainsi profondément de la documentalité. La documentarité, quant à elle, est déterminée par la réception :</p>
<blockquote>
<p>« la vérité documentaire se pose en contrepoint du vraisemblable fictionnel […] En invitant le spectateur à une sorte de confiance référentielle, l&rsquo;effet documentaire ainsi produit constitue un puissant vecteur de vraisemblable ». [@gaudreault1994, p. 15]</p>
</blockquote>
<p>Dans leur acception, la documentarité tient à la fois de la propriété et de la qualité — c&rsquo;est l&rsquo;<em>Eigenschaft</em> allemande. Elle n&rsquo;est pas liée à un acte d&rsquo;interprétation générique : elle occasionne un jugement de valeur. Les auteurs évoquent un <em>degré de documentarité</em>, c&rsquo;est-à-dire « un nombre plus ou moins grand d&rsquo;indices du réel » (p. 19) ; la documentarité représente donc une qualité documentaire quantifiable, mesurable.</p>
<p>Cette approche a été appliquée au domaine du Web par Sophie Beauparlant [-@beauparlant2017]. Analysant la documentarité du webdocumentaire dans les termes de Gaudreault et Marion, elle montre que l&rsquo;interface met en œuvre un jeu d&rsquo;écriture du réel qui s&rsquo;articule au contenu lui-même. Elle illustre la pertinence de leur proposition théorique en lui soumettant un cadre techno-sémiotique concret, confirmant l&rsquo;intuition que les interfaces numériques contribuent à produire une compétence de lecture.</p>
<p>Ni la documentalité ni la documentarité ne correspondent à une reformulation de ce qu&rsquo;est un document. Tout d&rsquo;abord, la notion de document est fonctionnelle et non substantielle, ainsi que l&rsquo;indiquent Otlet et Briet :</p>
<blockquote>
<p>« Le livre est le moyen d’enregistrement intégral de la pensée en vue de sa transmission […] comme instrument intellectuel le livre sert non seulement à énoncer des théories, mais à les construire ; non seulement à traduire la pensée, mais à la former ». [@otlet1934, p. 426]</p>
<p>« Un document est une preuve à l’appui d’un fait. […] Tout indice concret ou symbolique, conservé ou enregistré, aux fins de représenter, de reconstituer ou de prouver un phénomène ou physique ou intellectuel ». [@briet1951, p. 7]</p>
</blockquote>
<p>La plupart des théoriciens ultérieurs ont reconduit cette idée :</p>
<blockquote>
<p>« Un objet qui supporte de l&rsquo;information, qui sert à la communiquer et qui est durable […] un objet faisant fonction de mémoire pour une instance réceptrice ». [@meyriat1981]</p>
<p>« L&rsquo;évolution de la notion de “document” chez Otlet, Briet, Schürmeyer et les autres documentalistes a insisté de plus en plus sur tout ce qui fonctionne comme un document plutôt que sous une forme matérielle traditionnelle. Le passage au numérique semble rendre cette distinction encore plus importante ». [@buckland1997, p. 808]</p>
<p>« Un document est un objet intentionnel. Cela implique qu’il soit considéré pour ce qu’il signifie, et non pour ce qu’il est physiquement » [@bachimont2004, p. 192]</p>
</blockquote>
<p>Dans ce cadre, ce sont essentiellement les propriétés caractérisant le fonctionnement du document qui font l&rsquo;objet de nouvelles recherches. Or une propriété ne peut à elle seule résumer l&rsquo;ensemble des propriétés, ou alors elle constitue une nouvelle théorie. On notera que la documentalité selon Frohmann possède 4 aspects — « fonctionnalité, historicité, complexité, agentivité » — qui recoupent en partie la notion de document, ce qui interroge sur leur articulation. Quoiqu&rsquo;il en soit et pour toutes ces raisons, la documentarité n&rsquo;a pas vocation à réaliser en elle-même et par elle-même une vaste synthèse théorique qui reste, à notre sens, encore à venir.</p>
<p>La documentarité n&rsquo;est pas une propriété exclusive au document traditionnel : elle s&rsquo;applique à tout objet que l&rsquo;on analyserait dans une perspective documentaire. C&rsquo;est pourquoi elle peut s&rsquo;inscrire dans une démarche philosophique, ainsi que le fait Ron Day, ou plus spécifiquement info-communicationnelle, comme nous allons l&rsquo;illustrer ici avec la question des données.</p>
<h1 id="épistémologie-des-données">Épistémologie des données<a href="#épistémologie-des-données" class="hanchor" ariaLabel="Anchor">&#8983;</a> </h1>
<p>La notion de données est plus ancienne que son usage en informatique. Le terme lui-même n&rsquo;émerge qu&rsquo;au 17^e^ siècle, dans une phase du développement de l&rsquo;instrumentation scientifique qui voit se multiplier les objets d&rsquo;observation systématique. Bien que non formulée comme telle, c&rsquo;est l&rsquo;essor d&rsquo;une pratique de documentation du travail expérimental qui va faire naître la donnée. La logique en est déjà définie dans l&rsquo;Encyclopédie de Diderot et D&rsquo;Alembert :</p>
<blockquote>
<p>« Données, adj. pris subst. <em>terme de Mathématique</em>, qui signifie certaines choses ou quantités, qu&rsquo;on suppose être données ou connues, &amp; dont on se sert pour en trouver d&rsquo;autres qui sont inconnues, &amp; que l&rsquo;on cherche. Un problème ou une question renferme en général deux sortes de grandeurs, les <em>données</em> &amp; les cherchées, <em>data</em> &amp; <em>quæsita</em> ».</p>
</blockquote>
<p>Une certaine catégorie de <em>quæsita</em> procède de la certitude épistémologique, quasiment philosophique, suivant laquelle les données cherchées sont moins inconnues que non encore connues. Le mouvement des sciences sociales prédictives illustre à l&rsquo;extrême ce positionnement méthodologique. L&rsquo;autre catégorie correspond aux données inférées, produites par déduction à partir de données existantes. C&rsquo;est le principe du Web sémantique (<em>Linked Open Data</em>, LOD) : permettre d&rsquo;interroger des données exposées pour faciliter la production de nouvelles connaissances. La structuration et les métadonnées constituent les principaux leviers d&rsquo;enrichissement de ces données.</p>
<p>En France, la distinction entre document et données constitue un thème de recherche important au tournant du 20^e^ siècle, que les SIC ont abordé notamment par le concept de redocumentarisation. Ce terme porte à la fois l&rsquo;idée d&rsquo;un passage de l&rsquo;analogique au numérique et d&rsquo;une atomisation du document dans ses modes de production [@pedauque2007]. Le débat sur la pertinence de la notion de document est alors axé sur deux aspects. Le premier, toujours d&rsquo;actualité, concerne le bouleversement des valeurs documentaires traditionnelles, au premier rang desquelles la preuve. Le second aspect du débat a trait à la granularité ou à l&rsquo;échelle documentaire. Paul Otlet avait théorisé le dépassement du livre sur la base d&rsquo;une unité informationnelle abstraite, le biblion, et d&rsquo;une unité documentaire concrète, la fiche [@robert2015], une vision en partie vérifiée par la généralisation de l&rsquo;informatique bureautique.</p>
<p>En revanche, la science de l&rsquo;information anglo-saxonne se focalise plutôt sur la triade donnée-information-connaissance que sur le rapport entre données et documents. Une enquête très complète de Chaim Zins sur les approches conceptuelles de cette triade a montré que les définitions données pour <em>data</em> intègrent souvent le mot <em>record</em> ; celle de Michael Buckland en est un bon exemple : « The word “data” is commonly used to refer to records or recordings, statistical observations, collections of evidence » [@zins2007]. On peut y voir un phénomène de repli de l&rsquo;ancienne bibliothéconomie (américaine notamment) face à l&rsquo;émergence d&rsquo;une « science des données » (<em>data science</em>).</p>
<p>Si la promesse du Web sémantique avait incité les chercheurs à se poser de nouveau la question des différents niveaux de documents, l&rsquo;émergence des mégadonnées (ou <em>big data</em>) rend cette question en partie caduque, ou du moins la déplace. Il est acquis que la donnée est nécessairement plus petite que le document, ce qui facilite une économie de l&rsquo;information basée sur la raison computationnelle (calcul, recombinaison) avec les outils informatiques classiques développés durant la seconde moitié du 20^e^ siècle. En revanche, l&rsquo;agrégation de données sous forme de masses volumineuses et hétérogènes remet ces approches en question : les mégadonnées constituent des éléments infra-informationnels (selon l&rsquo;expression de Bruno Bachimont) qui excèdent les capacités d&rsquo;analyse à la fois méthodologiques et technologiques existantes. Le débat sur l&rsquo;articulation entre document et données se déplace alors des questions de granularité — influencées par l&rsquo;orientation positiviste des premières théories documentaires — à celles d&rsquo;architecture. De nouvelles technologies sont développées pour monter en charge sur l&rsquo;analyse brute, dont l&rsquo;apprentissage profond. Il est intéressant de noter que la logique sous-jacente à l&rsquo;analyse ne change pas ou peu : l&rsquo;esprit humain tend à rechercher des motifs réguliers dans des phénomènes désordonnés, nous développons simplement des techniques nouvelles adaptées à la complexification croissante de nos objets d&rsquo;étude.</p>
<p>À cette terminologie descriptive — données, cherchées, mégadonnées —, est venue s&rsquo;ajouter une terminologie plus conceptuelle. Dans les années 1990, les sociologues des sciences ont critiqué l&rsquo;usage du mot « donnée » pour désigner des objets en réalité construits, arrachés au terrain au prix d&rsquo;un temps et d&rsquo;efforts parfois considérables. Le mot « obtenue » est notamment suggéré par Bruno Latour comme une alternative souhaitable :</p>
<blockquote>
<p>« La tentation de l’idéalisme vient peut-être du mot même de <em>données</em> qui décrit aussi mal que possible ce sur quoi
s’appliquent les capacités cognitives ordinaires des érudits, des savants et des intellectuels. Il faudrait remplacer ce terme par celui, beaucoup plus réaliste, d&rsquo;<em>obtenues</em> et parler par conséquent de <em>bases d’obtenues</em>, de <em>sublata</em> plutôt que
de <em>data</em> ». [@latour2007, 609]</p>
</blockquote>
<p>Le mot <em>sublata</em> apparaît à diverses reprises dans les travaux de Latour mais il fait une occurrence particulièrement intéressante dans <em>Pandora&rsquo;s Hope</em>, où il est introduit suite à une remarque sur le rôle de la représentation graphique :</p>
<blockquote>
<p>« In order for the botanical and pedological data to be superposed on the same diagram later, these two bodies of reference must be compatible. One should never speak of “data”—what is given—but rather of <em>sublata</em>, that is, of “achievements” ». [@latour1999, 42]</p>
</blockquote>
<p>Les termes ne sont pas anodins. <em>Achievement</em> signifie accomplissement et sera traduit dans l&rsquo;édition française par « obtenues ». Avec ce mot, Latour insiste sur le fait que la connaissance est le fruit d&rsquo;une construction : pour lui, ce que nous appelons donnée est une information de nature processuelle. Quant à <em>sublata</em>, il découle des verbes latins <em>tollo</em> et <em>suffero</em>, qui signifient tour à tour élever, porter ou supporter. On peut y voir un lien avec la métaphore des nains se tenant sur des épaules de géants, très connue en sciences depuis sa reprise par Newton, et qui illustre la nature cumulative du savoir : nos accomplissements sont tributaires de ceux de nos prédécesseurs. La proposition de Latour sur les données nous semble liée à cette vision.</p>
<p>Plus récemment, Johanna Drucker a également avancé une alternative conceptuelle au mot « data ». Il s&rsquo;agit des « capta » :</p>
<blockquote>
<p>« co-dépendantes, constituées de manière relationnelle entre l&rsquo;observateur et les phénomènes observés, fondamentalement différentes du concept de la donnée comme phénomène indépendant de l&rsquo;observateur ». [trad. libre de @drucker2011, par. 50]</p>
</blockquote>
<p>Ceci prolonge son travail sur l&rsquo;interface, formulé dans des termes similaires :</p>
<blockquote>
<p>« Codépendance et contingence, l&rsquo;expérience performative de la connaissance produite dans la relation entre environnement et sujet, tels sont les termes qui définissent l&rsquo;interface interprétative ». [trad. libre de @drucker2011, p. 18]</p>
</blockquote>
<p>Comme Latour mais de façon plus systématique, Drucker s&rsquo;appuie la représentation graphique des résultats statistiques pour suggérer un glissement terminologique et conceptuel :</p>
<blockquote>
<p>« Croire que les données sont intrinsèquement quantitatives — évidentes, neutres sur le plan des valeurs et indépendantes de l&rsquo;observateur — exclut la possibilité de les concevoir comme qualitatives, co-dépendamment constituées — en d&rsquo;autres termes, de reconnaître que data sont des captas […] Je suggère que nous repensions fondamentalement les données comme des captas en termes d&rsquo;ambiguïté plutôt que de certitude, et que nous trouvions des moyens d’exprimer graphiquement la complexité de l’interprétation ». [trad. libre de @drucker2011, par. 49-50]</p>
</blockquote>
<p>Cette « complexité » dont parle Drucker provient notamment du fait que la représentation graphique hérite elle-même de modes d&rsquo;interprétation, ainsi que d&rsquo;expression : elle mobilise une ou plusieurs façons de penser, que nous devons prendre en compte. Les <em>capta</em> de Drucker traduisent le caractère cumulatif de la connaissance et la dimension processuelle de l&rsquo;information scientifique, tout comme les <em>sublata</em> de Latour.</p>
<h1 id="la-documentarité-des-données">La documentarité des données<a href="#la-documentarité-des-données" class="hanchor" ariaLabel="Anchor">&#8983;</a> </h1>
<p>Nous avons établi que la documentarité est une qualité perceptible pouvant faire l&rsquo;objet d&rsquo;un jugement de valeur et que la donnée est une construction qui porte la trace de modes d&rsquo;interprétation et d&rsquo;expression. Afin de croiser les réflexions sur la documentarité d&rsquo;une part et sur les données d&rsquo;autre part, nous nous penchons sur un exemple concret de mégadonnées ouvertes (intersection de l&rsquo;<em>open data</em> et du <em>big data</em>). Le portail Isidore moissonne, enrichit et expose des données bibliographiques issues de la recherche en SHS. Son utilisation nous permet de souligner trois logiques que nous pouvons relier aux éléments théoriques discutés précédemment.</p>
<p>La première logique est celle de la structuration. Elle est fondamentale, en ce que qu&rsquo;elle conditionne les deux autres. Toute écriture numérique organise sa propre énonciation computationnelle, laquelle fait l&rsquo;objet d&rsquo;une textualisation par l&rsquo;humain. Suite aux travaux de @goyet2017 et @collomb2017 sur la notion d&rsquo;architexte, nous définissons celle-ci comme une technologie intellectuelle qui permet une écriture de l&rsquo;écriture, mobilisant aussi bien la liste que l&rsquo;algorithme ou le balisage.</p>
<p>La donnée s&rsquo;inscrit dans ce schéma. Son encodage est la première caractéristique qui constitue l&rsquo;objet de notre regard interprétatif. Tout jeu de données est structuré suivant des règles syntaxiques spécifiques. L&rsquo;API d&rsquo;Isidore fournit des données en XML et en JSON, deux formats pensés pour le stockage et le transport de l&rsquo;information numérique. Dans les deux cas, les données sont considérablement enrichies par rapport à leur source, grâce au croisement de plusieurs référentiels. Il en résulte des fichiers texte dont le seul volume peut influencer notre appréciation de leur valeur informationnelle. Toutefois leur présentation diffère grandement. Le XML fait un usage classique du « blanc » (retours à la ligne et indentation) qui lui confère un aspect relativement lisible, avec une seule information par ligne. En revanche, le JSON livré par Isidore est « minifié », c&rsquo;est-à-dire que le blanc en est retiré à des fins d&rsquo;optimisation. Il en résulte un fichier qui, théoriquement, contient la même information, avec des délimiteurs moins lourds que le XML, mais qui est beaucoup moins lisible.</p>
<p>La deuxième logique observée est celle de l&rsquo;éditorialisation. Si la structuration est déjà une affaire de ligne éditoriale, l&rsquo;éditorialisation suppose des choix de médiation de la donnée qui dépassent le simple format de stockage et mobilisent des programmes de conversion (telles les feuilles XSLT) ainsi que des feuilles de style (par exemple en CSS). Lorsque des données sont affichées dans un navigateur Internet, celui-ci propose une mise en forme par défaut. Des langages différents (tel XML et JSON) peuvent être traités différemment. Ainsi, Firefox (Mozilla) n&rsquo;affiche pas directement le XML fourni par l&rsquo;API d&rsquo;Isidore mais une version sans balisage, ni retours à la ligne, ni indentation, au détriment de la lisibilité globale du fichier. En revanche, il propose une interface pour le JSON qui en facilite l&rsquo;usage (moteur de requêtes, affichage des entêtes, copie rapide). Isidore comprend par ailleurs une interface graphique qui change entièrement l&rsquo;expérience de la donnée par rapport à un usage « brut » via l&rsquo;API. Notre perception repose alors en grande partie sur les caractéristiques de l&rsquo;interface.</p>
<p>La troisième et dernière logique est celle de la réutilisation, qui correspond à la dimension combinatoire de la redocumentarisation, également exprimée par l&rsquo;idée de raison computationnelle. Toute écriture humaine sur ordinateur entraîne des opérations de lecture, calcul et écriture par la machine. La récupération des données est en partie conditionnée par leur exposition et en partie par les compétences d&rsquo;écriture du réutilisateur. Isidore propose un SPARQL <em>endpoint</em>, c&rsquo;est-à-dire une interface vers le jeu de données structurées suivant le principe du Web sémantique. Il permet une plus grande liberté d&rsquo;interrogation ainsi que l&rsquo;automatisation des requêtes : un langage de programmation comme Perl ou Python peut combiner SPARQL et expressions régulières pour extraire certains champs d&rsquo;un jeu de données, puis les inscrire dans un fichier avec une certaine syntaxe en vue d&rsquo;autres utilisations, par exemple une analyse statistique.</p>
<p>Cet objectif de réutilisation peut rencontrer plusieurs obstacles. Le plus évident est la piètre qualité de certaines sources, que l&rsquo;enrichissement ne permet pas de combler, et qui a un impact immédiat sur le traitement (absence d&rsquo;informations, mauvais nommage de champs). On voit ici une limite majeure des services basés sur le moissonnage, fortement dépendants de la qualité de la structuration en amont. Mais il faut également songer à la manipulation volontaire, beaucoup plus aisée via des ordinateurs qu&rsquo;aux temps de l&rsquo;imprimerie. L&rsquo;informatique a en effet généralisé un certain nombre de compétences éditoriales ; ainsi, comparée à celle de la monnaie, la falsification des statistiques apparaît comme triviale.</p>
<p>Dans l&rsquo;exemple que nous avons développé, les données fonctionnent essentiellement sur le mode de la documentarité « forte » au sens de Ron Day : c&rsquo;est le mécanisme traditionnel de la référence, qui repose sur la structuration et les métadonnées. Nous en jugeons la qualité en fonction de l&rsquo;adéquation à nos besoins d&rsquo;information ou de réutilisation, en référence à des normes (syntaxe, nomenclature). Ce que nous appelons degré de documentarité de ces données procède de leurs pouvoirs d&rsquo;expressions intrinsèques. Mais cette composante dispositionnelle est fortement contrainte par les affordances du support, en l&rsquo;occurrence le navigateur. Notre interaction avec les données définit leur capacité à manifester l&rsquo;évidence ; leur degré de documentarité dépend alors en partie de notre expérience informationnelle.</p>
<p>Par ailleurs, si on a pu dire que la documentarité en régime numérique n&rsquo;est pas évidente [@crozat2016], c&rsquo;est parce que l&rsquo;inscription des pouvoirs d&rsquo;expression se fait par un jeu d&rsquo;écriture plutôt difficile d&rsquo;accès. La culture technique n&rsquo;est pas partagée par tous les acteurs impliqués dans l&rsquo;exposition des données. En aval, c&rsquo;est la même chose : le passage par des jeux d&rsquo;écriture sophistiqués limite fortement la réutilisation. Interfaces, codes sources et algorithmes constituent un codage complexe qui n&rsquo;appartient pas encore à nos référentiels partagés, contrairement à des supports tels que le livre. La dimension cumulative et processuelle de l&rsquo;information est d&rsquo;autant plus complexe à interpréter. La part de documentarité « faible » rentre alors en jeu. Cet aspect transparaît peu dans le cas des données bibliographiques ; on peut imaginer d&rsquo;autres terrains pertinents pour prolonger ces réflexions, par exemple les portails <em>open data</em> des administrations publiques.</p>
<h1 id="conclusion">Conclusion<a href="#conclusion" class="hanchor" ariaLabel="Anchor">&#8983;</a> </h1>
<p>Ce qui fait document influence ce que nous faisons avec les documents. Cette logique s&rsquo;applique à tous les objets info-communicationnels. Les données, dont les problématiques en matière d&rsquo;épistémologie commencent à être réorientées en direction des questions interprétatives, illustrent particulièrement bien les logiques complexes qui président à leur valeur documentaire. En tentant d&rsquo;évaluer leur degré de documentarité, nous réalisons que celle-ci s&rsquo;exprime le plus souvent par des jeux d&rsquo;écriture qui, des principes de structuration aux possibilités de réutilisation en passant par les modes d&rsquo;éditorialisation, façonnent leurs contours et leur devenir. Il en résulte un triple enjeu de formation pour les projets liés aux données et mégadonnées en SHS, dont l&rsquo;appropriation reste suspendue au développement d&rsquo;une véritable culture technique. À cela, nous pensons que la théorie peut apporter une certaine contribution, notamment lorsque les concepts éclairent les continuités indiscutables entre questionnements anciens et actuels.</p>
<h1 id="références--">Références {-}<a href="#références--" class="hanchor" ariaLabel="Anchor">&#8983;</a> </h1>

      </div></div>

  
  
<div class="pagination">
    <div class="pagination__title">
        <span class="pagination__title-h"></span>
        <hr />
    </div>
    <div class="pagination__buttons">
        
        <span class="button previous">
            <a href="/notes/201910111200/">
                <span class="button__icon">←</span>
                <span class="button__text">L’écriture de l’histoire</span>
            </a>
        </span>
        
        
        <span class="button next">
            <a href="/notes/201909241543/">
                <span class="button__text">Encyclopédie</span>
                <span class="button__icon">→</span>
            </a>
        </span>
        
    </div>
</div>

  

  

</div>

  </div>

  
    <footer class="footer">
  <div class="footer__inner">
      <div class="copyright">
        <span>© Arthur Perret 2020 :: Site réalisé avec <a href="http://gohugo.io">Hugo</a> :: Thème Terminal par <a href="https://twitter.com/panr">panr</a></span>
      </div>
  </div>
</footer>

<script src="assets/main.js"></script>
<script src="assets/prism.js"></script>





  
</div>

</body>
</html>
