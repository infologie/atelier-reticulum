<!DOCTYPE html>
<html lang="fr">
<head>
  
    <title>Paul Otlet and the ultimate prospect of documentation :: Infologie</title>
  
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">
<meta name="description" content="\newthought{Paul Otlet (1868-1944) has left information science} a vast written legacy. Well-known works such as the Traité de documentation (Otlet, 1934) and the Répertoire bibliographique universel (Universal bibliographic directory) are not his only creations, and they should be studied along with his other productions. Otlet wrote and published many books and articles1. Exploring his archives and studying his various publications makes it possible to better understand his personality, the various influences on his way of thinking and his anticipations of the future." />
<meta name="keywords" content="" />
<meta name="robots" content="noodp" />
<link rel="canonical" href="/notes/201912190000/" />




<link rel="stylesheet" href="assets/style.css">

  <link rel="stylesheet" href="assets/red.css">



<link rel="stylesheet" href="style.css">


<link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/apple-touch-icon-144-precomposed.png">

  <link rel="shortcut icon" href="img/favicon/favicon.png">



<meta name="twitter:card" content="summary" />

  <meta name="twitter:site" content="@arthurperret" />

<meta name="twitter:creator" content="Olivier Le Deuff, Arthur Perret" />


<meta property="og:locale" content="fr" />
<meta property="og:type" content="article" />
<meta property="og:title" content="Paul Otlet and the ultimate prospect of documentation :: Infologie">
<meta property="og:description" content="\newthought{Paul Otlet (1868-1944) has left information science} a vast written legacy. Well-known works such as the Traité de documentation (Otlet, 1934) and the Répertoire bibliographique universel (Universal bibliographic directory) are not his only creations, and they should be studied along with his other productions. Otlet wrote and published many books and articles1. Exploring his archives and studying his various publications makes it possible to better understand his personality, the various influences on his way of thinking and his anticipations of the future." />
<meta property="og:url" content="/notes/201912190000/" />
<meta property="og:site_name" content="Paul Otlet and the ultimate prospect of documentation" />

  
    <meta property="og:image" content="img/favicon/favicon.png">
  

<meta property="og:image:width" content="2048">
<meta property="og:image:height" content="1024">


  <meta property="article:published_time" content="2019-12-19 00:00:00 &#43;0000 UTC" />












</head>
<body class="">


<div class="container center headings--one-size">

  <header class="header">
  <div class="header__inner">
    <div class="header__logo">
      <a href="/">
  <div class="logo">
    INFOLOGIE
  </div>
</a>

    </div>
    <div class="menu-trigger">menu</div>
  </div>
  
    <nav class="menu">
  <ul class="menu__inner menu__inner--desktop">
    
      
        
          <li><a href="/a-propos">À propos</a></li>
        
      
      
    

    
  </ul>

  <ul class="menu__inner menu__inner--mobile">
    
      
        <li><a href="/a-propos">À propos</a></li>
      
    
    
  </ul>
</nav>

  
</header>


  <div class="content">
    
<div class="post">
  <h1 class="post-title">
    <a href="/notes/201912190000/">Paul Otlet and the ultimate prospect of documentation</a></h1>
  <div class="post-meta">
    
      <span class="post-date">
        2019-12-19 
      </span>
    
    
    <span class="post-author">::
      Olivier Le Deuff, Arthur Perret
    </span>
    
  </div>

  

  

  

  <div class="post-content"><div>
        <p>\newthought{Paul Otlet (1868-1944) has left information science} a vast written legacy. Well-known works such as the <em>Traité de documentation</em> (Otlet, 1934) and the <em>Répertoire bibliographique universel</em> (Universal bibliographic directory) are not his only creations, and they should be studied along with his other productions. Otlet wrote and published many books and articles<sup id="fnref:1"><a href="#fn:1" class="footnote-ref" role="doc-noteref">1</a></sup>. Exploring his archives and studying his various publications makes it possible to better understand his personality, the various influences on his way of thinking and his anticipations of the future.</p>
<p>Otlet did not conceive documentation as fixed but as evolving [@ledeuff2014b]. According to him, documentation is not restricted to paper-based forms but embraces well and truly all documentary forms, including audiovisual. He also imagined future developments with new devices. Otlet laid down the successive evolutions of documentation as a way to better understand and anticipate what remains to be done to extend our knowledge. This prospective logic has attracted some misunderstanding and criticism. Otlet’s more daring projections were considered utopian, the work of an over-emphatic person with too many complex and unpractical ideas. His ambition to index all knowledge draws comparisons to Sisyphus or Don Quixote.</p>
<p>We believe that Otlet can best be understood by studying him back in the historical context of his time, with a benevolent look from the present. We can make use of his concepts and visions in a current perspective by studying the pathways to new models of knowledge that he contributed to creating.</p>
<p>In the first part of this paper, we present the relation between the concepts of documentation and hyperdocumentation, the latter which Otlet coined in <em>Traité de documentation</em> (1934). It is further explained by elements from his following book, <em>Monde, essai d’universalisme</em> (1935), in which he shows the ultimate prospect of documentation, science and society. In the second part, we show the proximity between Otlet’s work and current conceptions of transhumanism in view of his Mundaneum project.</p>
<h1 id="the-ultimate-stage-of-documentation">The ultimate stage of documentation<a href="#the-ultimate-stage-of-documentation" class="hanchor" ariaLabel="Anchor">&#8983;</a> </h1>
<p>For Otlet, documentation is related to the progress of knowledge, the evolution of science and techniques. He does not envision a frozen discipline and professional field. Conversely, he always tried to anticipate its future. In <em>Traité de documentation</em>, he described the different stages of the document’s consideration in human societies. He also recalled at the same time the importance of documents long before the concept of documentation was created. After all, for Otlet, human societies are linked to documents, something that Maurizio Ferraris describes as documentality [@ferraris2013].</p>
<h2 id="the-different-stages-of-documentation">The different stages of documentation<a href="#the-different-stages-of-documentation" class="hanchor" ariaLabel="Anchor">&#8983;</a> </h2>
<p>Hyperdocumentation appears as an interesting concept for the understanding of the new stakes of documentation. The concept was coined by Paul Otlet and appears in <em>Traité de Documentation</em> in 1934. Hyperdocumentation can be linked to other writings and works of Otlet, especially his subsequent book, <em>Monde, essai d’universalisme</em> in 1935: in it, he described the ultimate matters of science, documentation and society. Howewer, it is in the <em>Traité</em> that he explains the evolution of the relationship between humans and documents:</p>
<blockquote>
<p>“In the first stage, Man sees the Reality of the Universe by his own senses. Immediate, intuitive, spontaneous and unthinking knowledge. In the second stage, he reasons Reality and, combining his experience, generalising it, interpreting it, he makes a new representation of it. In the third stage, he introduces the Document which records what his senses have perceived and what his thought has constructed. At the fourth stage, he creates the scientific instrument and Reality then appears to be magnified, detailed, specified, another Universe successively reveals all its dimensions. In the fifth stage, the Document intervenes again and it is to directly record the perception provided by the instruments. Documents and instruments are so associated that there are no longer two distinct things, but only one: the Document-Instrument”. [@otlet1934, 429]</p>
</blockquote>
<p>The connection between document and instrument is very important in a digital humanities perspective: the fusion allows scientists to better understand their field and to better demonstrate their results. Further, Otlet describes the time of hyperdocumentation, the ultimate stage of documentation:</p>
<blockquote>
<p>“In the sixth stage, one stage further and all the senses having given rise to a proper development, a recording instrumentation having been established for each, new senses having come out of the primitive homogeneity and having been defined, while the spirit perfects its conception, in these conditions we glimpse Hyper-Intelligence. ‘Meaning-Perception-Document’ are things, concepts welded together. Visual documents and acoustic documents are completed by other documents, with touch, taste, fragrance and more. At this stage also the ‘insensitive’, the imperceptible, will become sensitive and perceptible through the tangible intermediary of the instrument-document. The irrational in its turn, all that is incommunicable and neglected, and because of that revolts and rises as it happens these days, the irrational will find its ‘expression’ by ways still unsuspected. And then it will really be the stage of Hyper-Documentation”. [@otlet1934, 429]</p>
</blockquote>
<p>“Hyper” in hyperdocumentation can be understood in different ways. It is the consequence of the massification of the documentary presence in the different organisations. But hyper can also be understood as the extension of the different kinds of documents (paper, audiovisual…). Moreover, hyperdocumention means the use of new methods of treatment (statistical machines) and new possibilities for creating links between documents (hyperdocument) [@ledeuff2019].</p>
<h2 id="hyperdocumentation-versus-hyperseparatism">Hyperdocumentation versus Hyperseparatism<a href="#hyperdocumentation-versus-hyperseparatism" class="hanchor" ariaLabel="Anchor">&#8983;</a> </h2>
<p>Paul Otlet believed that in its most advanced stage, documentation would be the best way to create links and to prevent division, which were unfortunately happening at full speed during his time:</p>
<blockquote>
<p>“As the world goes now, on the lines of hyper-separatism, there will soon be only documentation to establish regular and benevolent contact between man”. [@otlet1935, 387; translated by @rayward1975, 354]</p>
</blockquote>
<p>Otlet seeks to respond to hyperseparatism, which manifests itself mainly in two forms.</p>
<p>The scientific hyperseparatism concerns the multiple disciplinary and sub-disciplinary divisions which make more difficult the realisation of a global comprehension of the world, especially the work of synthesis. The Universal Decimal Classification (UDC) of Otlet and La Fontaine assumed that it was possible to understand things as a whole and to have a global vision of the different sections of knowledge. Paul Otlet’s fear was that the different disciplines would no longer work together towards a common goal. His book <em>Monde</em> clearly shows the desire to synthesize knowledge. The aim is to produce connections between the different fields of science to better understand the world and to contribute to the improvement of human societies. The UDC follows this logic and was designed not just as a classification system but as a tool to understand the different sections of knowledge. This is also present in the writings of Eugene Garfield, who worked on the links between science and documentation. In a 1955 paper [@garfield1955], he imagined how it could be possible to better understand science, its evolution and subdvisions thanks to new tools that would allow us a global understanding.</p>
<p>National hyperseparatism is another matter, on a larger scale. It leads humanity to divide instead of cohesion. Otlet favoured universal projects that brought out commonalities rather than differences. World conflicts are essentially the result of nationalist quarrels; Otlet sought to find ways out of them. This is apparent in his will to create international organisations for knowledge and peace between peoples, with the help of his friend La Fontaine as well as other close collaborators. For Otlet, the aim was to answer nationalism by supranationalism, taking the form of a “supranational cosmopolitan republic”, a project led by Henri Follin (1866-1918) and in which Otlet took part representing Belgium. The secretariat of this association was even hosted for a time at the <em>Mundaneum</em> at the Palais Mondial in Bruxelles. This is one context in which we see that Otlet wanted to elevate Belgium from the national level and inscribe in an international perspective. If Otlet imagined Bruxelles as the future centre for a league of nations, he would probably appreciate its choice as the city of the European Commission. In addition, Otlet tried to think about the ways to better communicate through speech between different people. He was very attentive to the projects for the development of common languages such as Esperanto and IDO.</p>
<p>For Otlet, the danger was that conflicts and internal quarrels always lead not only to global misunderstanding but to stagnation. In his mind, the journey had to continue, even though it may have looked like a direct path to his own hardships. Progress was not to be stopped.</p>
<h1 id="progress-between-augmented-man-and-transhumanism">Progress between augmented Man and Transhumanism<a href="#progress-between-augmented-man-and-transhumanism" class="hanchor" ariaLabel="Anchor">&#8983;</a> </h1>
<p>The desire of Otlet is to improve both man and society. For him, the aim of progress was to produce a better world. The initial goal of the Mundaneum was to build an index of all knowledge but this project went much further and defined a virtuous circle. Otlet defines progress in five ways, making sure to include the progression of the human being itself:</p>
<blockquote>
<p>“Summer Maine<sup id="fnref:2"><a href="#fn:2" class="footnote-ref" role="doc-noteref">2</a></sup> expressed the opinion that the normal state of societies, far from being progress, was stagnation. That is to say that at a point of equilibrium there is crystallisation, an adaptation as perfect as possible and then new circumstances are needed to put it back in motion. But the science factor has developed. It now seems quite difficult to eliminate or put it to sleep. Thus society, progress, science, have become terms expressing realities forming a cycle. PROGRESS.<br>
The progress of humanity is glimpsed in five different directions.</p>
<ol>
<li>By biological improvements, especially heredity.</li>
<li>By technical creations creating a favourable environment which, being transmissible, realises a kind of exothermic heredity.</li>
<li>By education: the progress of pedagogical and intellectual methods and pedagogical inheritance.</li>
<li>By the progress of intelligence in itself which creates science not only by discoveries and reasoning from certain axioms, but renews the idea of the principles themselves. The human mind has a creative activity.</li>
<li>By the progressive formation of a collective brain. Already the functions that would be assured are outlined: perception, through information; ideation through collective studies; memory, through documentation; impression, by the development of art; decisions by joint deliberations.<br>
All human history has been made of great emancipation of tradition. So when Jesus came to liberate the individual conscience from the grip of society, when serfdom, and later civil liberty, came to replace the ancient slavery that geniuses like Plato and Aristotle declared, however, conform to the nature of the man”. [@otlet1935]</li>
</ol>
</blockquote>
<p>These five areas of progress shape the definition of a “hyper-human” or even “trans-human” project. On the topic of the potential improvement of man, we must remember the proximity of Paul Otlet with the Nobel Prize in Medicine and eugenist Charles Richet, who lamented that men spend so much time fighting rather than seeking to improve their species [@richet1919]. Otlet’s aim combined this with his work on new devices which would help take better decisions without being imprisoned by personal interest and a deficit of knowledge.</p>
<h2 id="between-augmentation-theory-and-transhumanism">Between augmentation theory and transhumanism?<a href="#between-augmentation-theory-and-transhumanism" class="hanchor" ariaLabel="Anchor">&#8983;</a> </h2>
<p>Otlet envisioned the progress of humanity as correlated with that of science and documentation, so that man could reach a new status. Certainly, his Mundaneum project was an immeasurable and probably Promethean venture. The vision of an empowered Prometheus has influenced Otlet in more ways than one. It is not by chance that the painting of the symbolist artist Jean Delville (1867-1953) representing an all-powerful Prometheus was on the walls of the Palais Mondial. Jean Delville wanted his painting to be present in a place of knowledge: an institution such as a university or an organisation with a universal and scientific project. He found it in the Palais Mondial of Bruxelles; the painting now appears at the Université Libre de Bruxelles. Paul Otlet regularly produced drawings and diagrams to express his thoughts and he describes this fact in <em>Traité de Documentation</em> by referring precisely to Jean Delville:</p>
<blockquote>
<p>“Nowadays, writing is doubled by drawing. Why not learn how to draw as we know how to write. ‘Of all the exercises one can imagine provoking the spontaneity of thought, the most natural, the most logical and the most fruitful is the drawing (Jean Delville)’”. [@otlet1934, 78]</p>
</blockquote>
<p>This graphic and pictorial side is also found in Otlet’s personal papers, in which writing immediately brings out images and representations. For Delville, there is a desire to search for the “monumental” as Emilie Berger shows [@berger2015]. A “monumentality” can also be found in the drawings and writings of Paul Otlet. Each detail is connected to another. Each part of a project has to be linked with the others in a broader perspective. As a matter of fact, when he described the ultimate problem of documentation, it is clearly a Promethean man who appeared:</p>
<blockquote>
<p>“And here is the ultimate problem of documentation (technical and organisational). Man would no longer need documentation if he were assimilated to a being who had become omniscient, in the manner of God himself. To a lesser degree, remote instrumentation would be created combining radio, Röntgen rays, cinema and microscopic photography. All things in the universe, and all things of man, would be recorded remotely as they occurred. Thus would be established in the moving image of the world, its memory, its true double. Each one at a distance could read the passage which, enlarged and limited to the desired subject, would be projected on the individual screen. Thus, everyone in his chair could contemplate creation, in its entirety or in some of its parts”. [@otlet1935]</p>
</blockquote>
<p>We find in this excerpt the logic already present in <em>Traité de Documentation</em> with the ability to access documentation at a distance. But here, Otlet goes further than a description of the device. The “man-god”, such as Prometheus, is the “one who knows the most in the world”, just as Hesiod described the Titan [@briquel1978; @briquel1980]. However, Prometheus may not be a single man or a superman for Otlet but rather a system or a network of collective intelligence, because the needs of new devices to treat the sum of data and knowledge became primordial:</p>
<blockquote>
<p>“We must have a group of related machines that simultaneously or one after the other performs the following operations: transforming sound to writing; multiplying this writing as many times as required establishing documents in such a way that each data has its own individuality and its individuality in a relationship with others in the set, so that it may be referred where required; index of the category related to each data, punching documents according to its indices; automatically classifying these documents and placing them in a file; automatically retrieving documents to be consulted and presenting them either before the eyes or in a part of the machine having to do with additional entries mechanically handling at will any entered data to obtain new combinations of facts, new relationships of ideas, and new operations with the help of numbers. The machinery that will perform these seven desiderata will be an actual mechanical and collective brain”. [@otlet1934, 391; translated in @ledeuff2018]</p>
</blockquote>
<p>The interest for the mechanical and collective brain is shared with other anticipators including Herbert Georges Wells [@aracelitorresvargas2005; @rayward1983]. Wells was invited in 1937 during the World Congress of documentation in Paris. He gave a talk on “The idea of a permanent world encyclopaedia”. The article was published in 1937 and became part of a book with other essays, published in 1938 under the title “World Brain” [@wells1938].</p>
<p>To better grasp the definition of progress given by Otlet, we must bring together the ultimate goal of documentation and the ultimate goal of scientific knowledge:</p>
<blockquote>
<p>“The ultimate problem of scientific knowledge: to know so well all reality, its beings, its phenomena and its laws that it is possible to disintegrate all that exists, to reconstitute it, to order it in different ways. The ultimate problem of the technique: One man only having to push a button so that all the factories of the world, settled perfectly between them, start to produce all that is necessary for all humanity. The ultimate problem of the Society: Freedom creates divergences. In a state limit, more would not need to resort to others. Everyone could get everything he wanted by appealing directly to things alone, and dispensing with men. Thus the machine would have become the liberator of each, its operation being done by one and things being arranged in the right order for this alone”. [@otlet1935]</p>
</blockquote>
<p>We can compare this description with a drawing of Otlet in which he imagined the fusion of documents and instruments in a new device: the cosmoscope [@rayward1994; @vandenheuvel2011]. The cosmoscope is linked to the cosmograph, a device made to record all documents. With the cosmoscope, a single man can watch the whole universe and understand all of his parts. Moreover, the ultimate prospect of science described by Otlet shows a will to transform the universe and not only to understand it. The cosmoscope is but one device in his biggest project: the Mundaneum. Initially the continuation of the Répertoire Bibliographique Universel (RBU), it became more and more the core of all Otlet&rsquo;s initiatives regarding knowledge, pacific organisations and his project of “Cité mondiale” [@gresleri1982]. The Mundaneum is not only a centre designed to bring together all the knowledge of the word in the manner of a library [@vanacker2012]. It is a tool to change the world. If we keep in mind this fact, we can better understand what kind of centre (and center) it was meant to be.</p>
<h2 id="mundaneum-as-a-new-centre-of-the-universe">Mundaneum as a new centre of the universe<a href="#mundaneum-as-a-new-centre-of-the-universe" class="hanchor" ariaLabel="Anchor">&#8983;</a> </h2>
<p>In <em>Monde, essai d’universalisme</em>, Otlet explains that the Mundaneum is not only a centre for information but a universal centre:</p>
<blockquote>
<p>“It’s up to it to represent the design in the most complete way. A universal centre to which all humans can be attached, directly or through the intervention of their local, regional or national associations, the latter being themselves affiliated to their corresponding international association. Through the central Mundaneum, at one point of the globe will exist in the image and the total meaning of the world. An instrument to intellectuality acting distinctly from the achievements of the Economic, Social, Political, Religious, outside of them, but in reciprocal connection and cooperation with them. A monument where, by arranging itself, can accumulate ideas, feelings and efforts. Summary of the total, symbol of all symbols, prototype of all types of important things reconciled and in order, classification of classifications, documentation of documentation, home of homes, university of universities. It will be like the 101st wonder of the world, a monument to all the material and intellectual glories of the Universe; a sacred place inspiring great ideas and noble activities, treasure is the sum of duplication of all works of the mind, brought there as a contribution to science and universal organisation. ‘Pan’, ‘Omnium’ and ‘Everything’ building, a figurative testimony of the immense epic and magnificent adventure that humanity has pursued through the ages” [@otlet1935].</p>
</blockquote>
<p>We can better understand this description of the project of Mundaneum with the drawing of the <em>Species Mundaneum</em> (Fig. 1):</p>
<p><img src="../img/paul-otlet-and-the-ultimate-prospect-of-documentation-1.jpg" alt="Mundaneum species, les formes ou les types de Mundaneum à unir en réseau (forms and types of Mundaneum to be united in a network)."></p>
<p>Mundaneum is both a centre and a network. It is designed as an ideal for human societies in context of the creation of a new world. Mundaneum appears to be both a utopia and a heterotopia [@foucault1984]. It is the representation of an ideal world, which is in fact a utopia. But it is also the gathering of places that have the ability to show the world themselves, such as libraries, museums and atlases. Moreover, it is a new model of society that Paul Otlet wants to develop in a virtual fractal way. It is a new spirit of the world that he aims to infuse as if a new demiurge.</p>
<h1 id="conclusion">Conclusion<a href="#conclusion" class="hanchor" ariaLabel="Anchor">&#8983;</a> </h1>
<p>The study of Paul Otlet’s work shows that he is certainly a man of his time, seeking to fight against war and ignorance by proposing solutions to answer the problems that he and his contemporaries met during their own life. However, at the same time, he was considering changes and imagining mechanisms and institutions capable of bringing about a progression of humanity. His interest in technical devices and the possibilities of processing and categorising data explain the misunderstanding that he can sometimes provoke. In 1944, a collaborator, Valère Darchambeau tried to explain the thinking of Paul Otlet and his legacy:</p>
<blockquote>
<p>“Mr. Otlet’s mental audacities, his ‘utopias’ some would say, take on their full value when considered in terms of the super-fast mechanical brain that these machines constitute. This multiple and differentiated characterisation to which Mr. Otlet would like to end, practically unusable in a valid time by our slow and quickly put off human brains, is likely to be the ‘code’ through which statistical machines will soon reveal (in 5, 20, 50 or x years of unsuspected and unsuspected connections between things”.<sup id="fnref:3"><a href="#fn:3" class="footnote-ref" role="doc-noteref">3</a></sup></p>
</blockquote>
<p>His desire to bring man closer to knowledge also marks a form of possession that is so great it is ultimately possible for him to improve the world in which he finds himself. For Otlet, this global vision is based on essentially benevolent precepts to combat the evils of humanity. But the ultimate perspectives that he describes questions the different possibilites it could lead to. We must understand the Mundaneum as a work dedicated to knowledge but also as a cosmogony :</p>
<blockquote>
<p>“We have seen that it was not only temples that were thought to be situated at the ‘Centre of the World’, but that every holy place, every place that bore witness to an incursion of the sacred into profane space, was also regarded as a ‘centre’ These sacred spaces could also be constructed; but their construction was, in its way, a cosmogony-a creation of the world which is only natural since, as we have seen, the world was created in the beginning of an embryo, from a ‘centre’. Thus, for instance, the construction of the Vedic fire altar reproduced the creation of the world, and the altar itself was a microcosm, an <em>imago mundi</em>”. [@eliade1961, 51-52]</p>
</blockquote>
<p>The work of Otlet must be understood in this sense, as it is a matter of connecting all his visions and creations within a Mundaneum and its derivatives. The ultimate prospect of documentation may come soon, but we have to imagine all the kinds of Mundaneum that go with it.</p>
<h2 id="references">References<a href="#references" class="hanchor" ariaLabel="Anchor">&#8983;</a> </h2>
<section class="footnotes" role="doc-endnotes">
<hr>
<ol>
<li id="fn:1" role="doc-endnote">
<p>W. Boyd Rayward has compiled a bibliography of Paul Otlet’s writings from 1882 to 1944: <a href="http://hdl.handle.net/2142/652">http://hdl.handle.net/2142/652</a> <a href="#fnref:1" class="footnote-backref" role="doc-backlink">&#x21a9;&#xfe0e;</a></p>
</li>
<li id="fn:2" role="doc-endnote">
<p>Henry Summer Maine (1822-1888) was a famous British jurist. <a href="#fnref:2" class="footnote-backref" role="doc-backlink">&#x21a9;&#xfe0e;</a></p>
</li>
<li id="fn:3" role="doc-endnote">
<p>Archives of Mundaneum. PP P0 462. <a href="#fnref:3" class="footnote-backref" role="doc-backlink">&#x21a9;&#xfe0e;</a></p>
</li>
</ol>
</section>

      </div></div>

  
  
<div class="pagination">
    <div class="pagination__title">
        <span class="pagination__title-h"></span>
        <hr />
    </div>
    <div class="pagination__buttons">
        
        <span class="button previous">
            <a href="/notes/202001011145/">
                <span class="button__icon">←</span>
                <span class="button__text">Sémiotique des fluides</span>
            </a>
        </span>
        
        
        <span class="button next">
            <a href="/notes/201912180000/">
                <span class="button__text">Writing Documentarity</span>
                <span class="button__icon">→</span>
            </a>
        </span>
        
    </div>
</div>

  

  

</div>

  </div>

  
    <footer class="footer">
  <div class="footer__inner">
      <div class="copyright">
        <span>© Arthur Perret 2020 :: Site réalisé avec <a href="http://gohugo.io">Hugo</a> :: Thème Terminal par <a href="https://twitter.com/panr">panr</a></span>
      </div>
  </div>
</footer>

<script src="assets/main.js"></script>
<script src="assets/prism.js"></script>





  
</div>

</body>
</html>
