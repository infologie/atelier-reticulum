<!DOCTYPE html>
<html lang="fr">
<head>
  
    <title>All papers are data papers: from open principles to digital methods :: Infologie</title>
  
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">
<meta name="description" content="\marginnote[0.4cm]{\textbf{Proposition acceptée} au colloque \href{https://dh2020.adho.org/}{DH 2020 \textit{carrefours/intersections}} (annulé en présentiel).}How do we bridge the gap between ambitious global schemes, such as Paul Otlet’s “Aims of documentation” [@otlet1934] or the FAIR data principles [@wilkinson2016], and existing information practices? We describe the theoretical basis and practical steps for a subject-oriented approach to this problem, examining data-related expectations through the lens of documentarity.
In 1934, Belgian bibliographer Paul Otlet published a Treaty of documentation in which he outlined the “Aims of documentation”:" />
<meta name="keywords" content="" />
<meta name="robots" content="noodp" />
<link rel="canonical" href="/notes/202002180000/" />




<link rel="stylesheet" href="assets/style.css">

  <link rel="stylesheet" href="assets/red.css">



<link rel="stylesheet" href="style.css">


<link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/apple-touch-icon-144-precomposed.png">

  <link rel="shortcut icon" href="img/favicon/favicon.png">



<meta name="twitter:card" content="summary" />

  <meta name="twitter:site" content="@arthurperret" />

<meta name="twitter:creator" content="Arthur Perret, Olivier Le Deuff" />


<meta property="og:locale" content="fr" />
<meta property="og:type" content="article" />
<meta property="og:title" content="All papers are data papers: from open principles to digital methods :: Infologie">
<meta property="og:description" content="\marginnote[0.4cm]{\textbf{Proposition acceptée} au colloque \href{https://dh2020.adho.org/}{DH 2020 \textit{carrefours/intersections}} (annulé en présentiel).}How do we bridge the gap between ambitious global schemes, such as Paul Otlet’s “Aims of documentation” [@otlet1934] or the FAIR data principles [@wilkinson2016], and existing information practices? We describe the theoretical basis and practical steps for a subject-oriented approach to this problem, examining data-related expectations through the lens of documentarity.
In 1934, Belgian bibliographer Paul Otlet published a Treaty of documentation in which he outlined the “Aims of documentation”:" />
<meta property="og:url" content="/notes/202002180000/" />
<meta property="og:site_name" content="All papers are data papers: from open principles to digital methods" />

  
    <meta property="og:image" content="img/favicon/favicon.png">
  

<meta property="og:image:width" content="2048">
<meta property="og:image:height" content="1024">


  <meta property="article:published_time" content="2020-02-18 00:00:00 &#43;0000 UTC" />












</head>
<body class="">


<div class="container center headings--one-size">

  <header class="header">
  <div class="header__inner">
    <div class="header__logo">
      <a href="/">
  <div class="logo">
    INFOLOGIE
  </div>
</a>

    </div>
    <div class="menu-trigger">menu</div>
  </div>
  
    <nav class="menu">
  <ul class="menu__inner menu__inner--desktop">
    
      
        
          <li><a href="/a-propos">À propos</a></li>
        
      
      
    

    
  </ul>

  <ul class="menu__inner menu__inner--mobile">
    
      
        <li><a href="/a-propos">À propos</a></li>
      
    
    
  </ul>
</nav>

  
</header>


  <div class="content">
    
<div class="post">
  <h1 class="post-title">
    <a href="/notes/202002180000/">All papers are data papers: from open principles to digital methods</a></h1>
  <div class="post-meta">
    
      <span class="post-date">
        2020-02-18 
      </span>
    
    
    <span class="post-author">::
      Arthur Perret, Olivier Le Deuff
    </span>
    
  </div>

  

  

  

  <div class="post-content"><div>
        <p>\marginnote[0.4cm]{\textbf{Proposition acceptée} au colloque \href{https://dh2020.adho.org/}{DH 2020 \textit{carrefours/intersections}} (annulé en présentiel).}How do we bridge the gap between ambitious global schemes, such as Paul Otlet’s “Aims of documentation” [@otlet1934] or the FAIR data principles [@wilkinson2016], and existing information practices? We describe the theoretical basis and practical steps for a subject-oriented approach to this problem, examining data-related expectations through the lens of documentarity.</p>
<p>In 1934, Belgian bibliographer Paul Otlet published a <em>Treaty of documentation</em> in which he outlined the “Aims of documentation”:</p>
<blockquote>
<p>“Universal as to their purpose; reliable and true; complete; fast; up to date; easy to obtain; collected in advance and ready to be communicated; made available to the greatest number of people.” [@otlet1934, 6]</p>
</blockquote>
<p>In 2016, the FAIR principles were published along similar lines:</p>
<blockquote>
<p>“To be Findable; to be Accessible; to be Interoperable; to be Reusable.” [@wilkinson2016, 4]</p>
</blockquote>
<p>They differ in some ways: Otlet viewed the Aims as a whole, with openness as a critical element, while FAIR is modular and not necessarily synonymous with open data. But more importantly, they both describe a plan which is meant to precede and guide implementation. Otlet&rsquo;s Aims are broken down into goals related to the actual “<em>biblio-technie</em>” or “<em>bibliothéconomie</em>” [@otlet1934, 372-375]; similarly, each of the 4 components of FAIR is itself divided in 4 sub-components which delve into technical matters (e.g. data vs. metadata). These are actionable steps to be applied in the field, which is where trouble begins.</p>
<p>During and after his time, close collaborators and distant peers alike noted the gap between Otlet&rsquo;s ambitions and what he was able to achieve: Valère Darchambeau commented on “Mr. Otlet’s mental audacities, his utopias some would say” (Mundaneum archives, PP P0 462); Suzanne Briet called him ironically “the magus” of documentation. Indeed, he had a major impact on the institutionalization of documentation—the development of Library and information science (LIS) in Europe owes much to section 4 of his <em>Treaty</em>—but his work on the relationship between subject and knowledge was largely neglected. The techno-semiotic mediations of information have been far less studied in LIS than human ones; we can arguably trace this back to Otlet&rsquo;s incomplete legacy. Conversely, the implementation of FAIR principles quickly raised the issues of user experience, expectations and metrics:</p>
<blockquote>
<p>“FAIRness is aspirational, yet the means of reaching it may be defined by increased adherence to measurable indicators . . . metrics that reflect the expectations of particular communities.” [@wilkinson2017, 1-2]</p>
</blockquote>
<p>The interface between person and information seems much thinner for computer-held data than for library books. While this is not actually true (mediations have simply shifted towards human-computer interaction), it means that the feasibility of principles is challenged almost immediately by subjective experience. Data may be FAIR but people may differ: they do not all work on the same data or with the same mindset and therefore have different expectations. This shapes the way we assess data within the framework of documentation and therefore its value to us—its documentarity.</p>
<p>Documentarity is the product of interdisciplinary theoretical work, at the intersection between ontology, documentation and linguistics. The first of these two influences have been studied: documentarity can be seen as a philosophy of evidence based on documentation [@day2019] and also as the quantifiable documentary quality of things, with applications to digital documents and data [@perret2019c]. Here, we examine the third influence: how linguistics contribute to documentarity as an epistemological proposal which at the core focuses on the reception of information. We show that documentarity is linked to several works: Roman Jakobson&rsquo;s “<em>literaturnost</em>”, which in French (“<em>littérarité</em>”) [@jakobson1977, 16] is very close to documentarity (“<em>documentarité</em>”); Hans Robert Jauss&rsquo; adaptation of horizons of expectation (“<em>Erwartungshorizont</em>”) to literature [@jauss1970]; the shape of enunciation with Mary-Ann Caws “architexture” [@caws1981, 10] and Roger Laufer&rsquo;s “<em>scripturation</em>” [@laufer1986, 75].</p>
<p>This array of concepts is dense but its purpose is coherent: we draw from the phenomenology of the reading process to make better sense of the way we assess computer-held data. Our methodology is to track the embodiment of thought in technological mediations, especially in writing. The usefulness of such an approach has been described for the study of information as experience [@gorichanaz2017]. We argue that the way we perceive the documentarity of data is shaped by our horizons of expectation, especially previous experience of genre-based rules which me must establish if we wish to prevent global principles from falling into abstraction as soon as they enter the field.</p>
<p>In this perspective, digital notebooks form a stimulating case study, highly relevant to the conference&rsquo;s theme on open data. They relate to a tradition and to new practices (data science, data papers). We analyze the way data is presented and interacted with in R, Python and Javascript-based notebooks, and we observe a reflexive impact on our perception of documentarity: it allows us to relate more practically to the intellectual framework behind Otlet&rsquo;s “Aims of documentation” and the FAIR principles, which could improve their adoption. Through reproducibility and replicability, the practice of the notebook informs us on the relationship between data and truth. It also underlines the status of text as the most basic and universal type of data in science: the way text is handled in notebooks (lightweight markup languages, integration of standards, automation) shifts our perception of ‘text’ to ‘textual data’. This is independent from the field of study: we suggest that any research built from plain text can be considered a data paper and that extending “FAIRness” to scientific writing in general would be an epistemological breakthrough in scientific communication.</p>
<h1 id="references--">References {-}<a href="#references--" class="hanchor" ariaLabel="Anchor">&#8983;</a> </h1>

      </div></div>

  
  
<div class="pagination">
    <div class="pagination__title">
        <span class="pagination__title-h"></span>
        <hr />
    </div>
    <div class="pagination__buttons">
        
        <span class="button previous">
            <a href="/notes/202002201405/">
                <span class="button__icon">←</span>
                <span class="button__text">Reticulum - Interventions designers</span>
            </a>
        </span>
        
        
        <span class="button next">
            <a href="/notes/202002170000/">
                <span class="button__text">Cartographie de Paul Otlet sur le Web</span>
                <span class="button__icon">→</span>
            </a>
        </span>
        
    </div>
</div>

  

  

</div>

  </div>

  
    <footer class="footer">
  <div class="footer__inner">
      <div class="copyright">
        <span>© Arthur Perret 2020 :: Site réalisé avec <a href="http://gohugo.io">Hugo</a> :: Thème Terminal par <a href="https://twitter.com/panr">panr</a></span>
      </div>
  </div>
</footer>

<script src="assets/main.js"></script>
<script src="assets/prism.js"></script>





  
</div>

</body>
</html>
